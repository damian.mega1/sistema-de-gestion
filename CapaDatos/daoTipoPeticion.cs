﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using CapaEntidades;

namespace CapaDatos
{
    public class daoTipoPeticion
    {
        public static List<entTipoPeticion> ListarTiposPeticion()
        {
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<entTipoPeticion> lista = null;

            try
            {
                Conexion cn = new Conexion();
                SqlConnection cnx = cn.conectar();
                cmd = new SqlCommand("ListarTiposPeticion", cnx);
                cmd.CommandType = CommandType.StoredProcedure;
                cnx.Open();
                dr = cmd.ExecuteReader();
                lista = new List<entTipoPeticion>();
                while (dr.Read())
                {
                    entTipoPeticion c = new entTipoPeticion();
                    c.idTipoPeticion = Convert.ToInt32(dr["idTipo"].ToString());
                    c.Descripcion = dr["Descripcion"].ToString();
                    lista.Add(c);
                }
            }
            catch (Exception e)
            {
                lista = null;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return lista;
        }
    }
}
