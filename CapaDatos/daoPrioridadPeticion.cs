﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CapaEntidades;
using System.Data.SqlClient;
using System.Data;

namespace CapaDatos
{
    public class daoPrioridadPeticion
    {
        public static List<entPrioridadPeticion> ListarPrioridadesPeticion()
        {
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<entPrioridadPeticion> lista = null;

            try
            {
                Conexion cn = new Conexion();
                SqlConnection cnx = cn.conectar();
                cmd = new SqlCommand("ListarPrioridadesPeticion", cnx);
                cmd.CommandType = CommandType.StoredProcedure;
                cnx.Open();
                dr = cmd.ExecuteReader();
                lista = new List<entPrioridadPeticion>();
                while (dr.Read())
                {
                    entPrioridadPeticion c = new entPrioridadPeticion();
                    c.idPrioridadPeticion = Convert.ToInt32(dr["idPrioridad"].ToString());
                    c.Descripcion = dr["Descripcion"].ToString();
                    lista.Add(c);
                }
            }
            catch (Exception e)
            {
                lista = null;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return lista;
        }
    }
}
