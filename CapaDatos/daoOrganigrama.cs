﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CapaEntidades;
using System.Data.SqlClient;
using System.Data;

namespace CapaDatos
{
    public class daoOrganigrama
    {
        public static List<entOrganigrama> ListarOrganigrama()
        {
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<entOrganigrama> lista = null;

            try
            {
                Conexion cn = new Conexion();
                SqlConnection cnx = cn.conectar();
                cmd = new SqlCommand("ListarOrganigrama", cnx);
                cmd.CommandType = CommandType.StoredProcedure;
                cnx.Open();
                dr = cmd.ExecuteReader();
                lista = new List<entOrganigrama>();
                while (dr.Read())
                {
                    entOrganigrama c = new entOrganigrama();
                    c.idOrganigramaSector = Convert.ToInt32(dr["idOrganigramaSector"].ToString());
                    c.refPadre = Convert.ToInt32(dr["refPadre"].ToString());
                    c.refDestino = Convert.ToInt32(dr["refDestino"].ToString());
                    c.cAbNombreSector = dr["cAbNombreSector"].ToString();
                    c.cNombreSector = dr["cNombreSector"].ToString();
                    c.cRegionMinisterio = dr["cRegionMinisterio"].ToString();
                    c.cGrupoMinisterio = dr["cGrupoMinisterio"].ToString();
                    c.bEsProvincia = Convert.ToBoolean(dr["bEsProvincia"].ToString());
                    c.bActivo = Convert.ToBoolean(dr["bActivo"].ToString());
                    c.bVisible = Convert.ToBoolean(dr["bVisible"].ToString());
                    lista.Add(c);
                }
            }
            catch (Exception e)
            {
                lista = null;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return lista;
        }
        public static entOrganigrama BuscarOrganigrama(int idOrganigramaSector)
        {
            entOrganigrama obj = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                Conexion cn = new Conexion();
                SqlConnection cnx = cn.conectar();
                cmd = new SqlCommand("BuscarOrganigrama", cnx);
                cmd.Parameters.AddWithValue("@idOrganigramaSector", idOrganigramaSector);
                cmd.CommandType = CommandType.StoredProcedure;
                cnx.Open();
                dr = cmd.ExecuteReader();
                obj = new entOrganigrama();
                dr.Read();
                obj.idOrganigramaSector = Convert.ToInt32(dr["idOrganigramaSector"].ToString());
                obj.refPadre = Convert.ToInt32(dr["refPadre"].ToString());
                obj.refDestino = Convert.ToInt32(dr["refDestino"].ToString());
                obj.cAbNombreSector = dr["cAbNombreSector"].ToString();
                obj.cNombreSector = dr["cNombreSector"].ToString();
                obj.cRegionMinisterio = dr["cRegionMinisterio"].ToString();
                obj.cGrupoMinisterio = dr["cGrupoMinisterio"].ToString();
                obj.bEsProvincia = Convert.ToBoolean(dr["bEsProvincia"].ToString());
                obj.bActivo = Convert.ToBoolean(dr["bActivo"].ToString());
                obj.bVisible = Convert.ToBoolean(dr["bVisible"].ToString());
            }
            catch (Exception e)
            {
                obj = null;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return obj;
        }
    }
}
