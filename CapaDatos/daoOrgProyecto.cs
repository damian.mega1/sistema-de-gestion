﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CapaEntidades;
using System.Data.SqlClient;
using System.Data;

namespace CapaDatos
{
    public class daoOrgProyecto
    {
        public static List<entOrgProyecto> ListarOrgProyecto()
        {
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<entOrgProyecto> lista = null;

            try
            {
                Conexion cn = new Conexion();
                SqlConnection cnx = cn.conectar();
                cmd = new SqlCommand("ListarOrgProyecto", cnx);
                cmd.CommandType = CommandType.StoredProcedure;
                cnx.Open();
                dr = cmd.ExecuteReader();
                lista = new List<entOrgProyecto>();
                while (dr.Read())
                {
                    entOrgProyecto c = new entOrgProyecto();
                    c.idOrganigramaSector = Convert.ToInt32(dr["idOrganigramaSector"].ToString());
                    c.idProyecto = Convert.ToInt32(dr["idProyecto"].ToString());
                    c.Nombre = dr["Nombre"].ToString();
                    lista.Add(c);
                }
            }
            catch (Exception e)
            {
                lista = null;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return lista;
        }
    }
}
