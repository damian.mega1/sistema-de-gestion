﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CapaEntidades;
using System.Data.SqlClient;
using System.Data;

namespace CapaDatos
{
    public class daoProyecto
    {
        public static List<entProyecto> ListarProyectos()
        {
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<entProyecto> lista = null;

            try
            {
                Conexion cn = new Conexion();
                SqlConnection cnx = cn.conectar();
                cmd = new SqlCommand("ListarProyectos", cnx);
                cmd.CommandType = CommandType.StoredProcedure;
                cnx.Open();
                dr = cmd.ExecuteReader();
                lista = new List<entProyecto>();
                while (dr.Read())
                {
                    entProyecto c = new entProyecto();
                    c.idProyecto = Convert.ToInt32(dr["idProyecto"].ToString());
                    c.Nombre = dr["Nombre"].ToString();
                    c.Descripcion = dr["Descripcion"].ToString();
                    c.refJefeProyecto = Convert.ToInt32(dr["refJefeProyecto"].ToString());
                    c.refOrganigramaSectores = Convert.ToInt32(dr["refOrganigramaSectores"].ToString());
                    lista.Add(c);
                }
            }
            catch (Exception e)
            {
                lista = null;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return lista;
        }
        public static int AgregarProyecto(entProyecto obj)
        {
            int Indicador = 0;
            SqlCommand cmd = null;
            try
            {
                Conexion cn = new Conexion();
                SqlConnection cnx = cn.conectar();
                cmd = new SqlCommand("AgregarProyecto", cnx);
                cmd.Parameters.AddWithValue("@Nombre", obj.Nombre);
                cmd.Parameters.AddWithValue("@Descripcion", obj.Descripcion);
                cmd.Parameters.AddWithValue("@refJefeProyecto", obj.refJefeProyecto);
                cmd.Parameters.AddWithValue("@refOrganigramaSectores", obj.refOrganigramaSectores);
                cmd.CommandType = CommandType.StoredProcedure;
                cnx.Open();
                cmd.ExecuteNonQuery();
                Indicador = 1;
            }
            catch (Exception e)
            {
                Indicador = 0;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return Indicador;
        }
        public static entProyecto BuscarProyecto(int idProyecto)
        {
            entProyecto obj = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                Conexion cn = new Conexion();
                SqlConnection cnx = cn.conectar();
                cmd = new SqlCommand("BuscarProyecto", cnx);
                cmd.Parameters.AddWithValue("@IdProyecto", idProyecto);
                cmd.CommandType = CommandType.StoredProcedure;
                cnx.Open();
                dr = cmd.ExecuteReader();
                obj = new entProyecto();
                dr.Read();
                obj.idProyecto = Convert.ToInt32(dr["idProyecto"].ToString());
                obj.Nombre = dr["Nombre"].ToString();
                obj.Descripcion = dr["Descripcion"].ToString();
                obj.refJefeProyecto = Convert.ToInt32(dr["refJefeProyecto"].ToString());
                obj.refOrganigramaSectores = Convert.ToInt32(dr["refOrganigramaSectores"].ToString());
            }
            catch (Exception e)
            {
                obj = null;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return obj;
        }
        public static int ModificarProyecto(entProyecto obj)
        {
            int Indicador = 0;
            SqlCommand cmd = null;
            try
            {
                Conexion cn = new Conexion();
                SqlConnection cnx = cn.conectar();
                cmd = new SqlCommand("ModificarProyecto", cnx);
                cmd.Parameters.AddWithValue("@idProyecto", obj.idProyecto);
                cmd.Parameters.AddWithValue("@Nombre", obj.Nombre);
                cmd.Parameters.AddWithValue("@Descripcion", obj.Descripcion);
                cmd.Parameters.AddWithValue("@refJefeProyecto", obj.refJefeProyecto);
                cmd.Parameters.AddWithValue("@refOrganigramaSectores", obj.refOrganigramaSectores);
                cmd.CommandType = CommandType.StoredProcedure;
                cnx.Open();
                cmd.ExecuteNonQuery();
                Indicador = 1;
            }
            catch (Exception e)
            {
                Indicador = 0;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return Indicador;
        }


        public static List<entProyecto> ListarProyectosAvances()
        {
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<entProyecto> lista = null;

            try
            {
                Conexion cn = new Conexion();
                SqlConnection cnx = cn.conectar();
                cmd = new SqlCommand("ListarProyectosAvances", cnx);
                cmd.CommandType = CommandType.StoredProcedure;
                cnx.Open();
                dr = cmd.ExecuteReader();
                lista = new List<entProyecto>();
                while (dr.Read())
                {
                    entProyecto c = new entProyecto();
                    c.idProyecto = Convert.ToInt32(dr["idProyecto"].ToString());
                    c.Nombre = dr["Nombre"].ToString();                   
                    c.PorcentajeAvance = Convert.ToInt32(dr["PorcentajeAvance"].ToString());
                   
                    lista.Add(c);
                }
            }
            catch (Exception e)
            {
                lista = null;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return lista;
        }
    }
}
