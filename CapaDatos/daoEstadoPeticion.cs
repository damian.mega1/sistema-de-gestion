﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CapaEntidades;
using System.Data.SqlClient;
using System.Data;

namespace CapaDatos
{
    public class daoEstadoPeticion
    {
        public static List<entEstadoPeticion> ListarEstadosPeticion()
        {
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<entEstadoPeticion> lista = null;

            try
            {
                Conexion cn = new Conexion();
                SqlConnection cnx = cn.conectar();
                cmd = new SqlCommand("ListarEstadosPeticion", cnx);
                cmd.CommandType = CommandType.StoredProcedure;
                cnx.Open();
                dr = cmd.ExecuteReader();
                lista = new List<entEstadoPeticion>();
                while (dr.Read())
                {
                    entEstadoPeticion c = new entEstadoPeticion();
                    c.idEstadoPeticion = Convert.ToInt32(dr["idEstado"].ToString());
                    c.Descripcion = dr["Descripcion"].ToString();
                    lista.Add(c);
                }
            }
            catch (Exception e)
            {
                lista = null;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return lista;
        }
    }
}
