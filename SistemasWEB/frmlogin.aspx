﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmlogin.aspx.cs" Inherits="MantenedorWEB.frmlogin2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<link href="estilos.css" rel="stylesheet" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Gestor de Tareas</title>
</head>
<body>
    <form id="form1" runat="server">
        <div id="abrigo_general">
    
        
        <td>
        <div id="banner">
            
            </div>
        </td>

       </div>

<div class="login-form">
    <h1>
    <asp:Image ID="imgLogin" runat="server" ImageUrl="~/Imagenes/user_login_128.png" Width="56px" Height="56px" />&nbsp;&nbsp; Inicio de Sesion</h1>
     <div class="form-group ">
         <!--<input type="text" class="form-control" placeholder="Username " id="txtUsuario">-->
         <asp:TextBox ID="txtUsuario" CssClass="form-control"  runat="server"></asp:TextBox>
       <i class="fa fa-user"></i>
     </div>
     <div class="form-group log-status">
       <!--<input type="password" class="form-control" placeholder="Password" id="txtPass">-->
       <asp:TextBox ID="txtPass" CssClass="form-control" runat="server" TextMode="Password"></asp:TextBox>
       <i class="fa fa-lock"></i>
     </div>
      
     <br />
      <asp:Label ID="lblError" runat="server" ForeColor="Red" Visible="False"></asp:Label>
     <!-- <span class="alert">
     <br />
     <br />
     <br />
     Invalid Credentials</span>
      <a class="link" href="#">Lost your password?</a>
     <button type="button" class="log-btn" id="btnAceptar" >Log in</button>-->
     
    
          <asp:Button ID="btnAceptar" CssClass="log-btn" runat="server" OnClick="btnAceptar_Click" Text="Aceptar"  />
     
    
   </div>


<script class="cssdeck" src="//cdnjs.cloudflare.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
    </form>
</body>
</html>
