﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmProyectos.aspx.cs" Inherits="MantenedorWEB.frmProyectos" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <h2>Proyectos</h2>
            <div id="contenido_izquierdo">

                <%--<table>
                <tr>
                    <td align="left">
                    <asp:Button ID="btnAgregar" runat="server" 
                    Text="Agregar" Width="80px" onclick="btnAgregar_Click"  /></td>
                    <td></td>
                    <td align="right">
                    <asp:Button ID="btnModificar" runat="server" 
                    Text="Modificar" Width="80px" onclick="btnModificar_Click"  /></td>
                </tr>            
            </table>--%>
                <%
                    System.Collections.Generic.List<CapaEntidades.entOrgProyecto> lista = CapaNegocio.negOrgProyecto.ListarOrgProyecto();
                    if (lista != null)
                    {
                        foreach (CapaEntidades.entOrgProyecto obj in lista)
                        {
                            hplcategoria.Text = obj.Nombre;
                            if (obj.idProyecto != 0)
                            {
                                hplcategoria.NavigateUrl = "frmPrincipal.aspx?cat=" + obj.idProyecto;
                            }
                %>
                <div class="etiqueta">
                    <asp:HyperLink ID="hplcategoria" runat="server" >[hplcategoria]</asp:HyperLink>
                </div>
                <%
                    }
                }
                %>
            </div>
    </form>
</body>
</html>
