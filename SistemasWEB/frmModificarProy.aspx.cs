﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using CapaEntidades;
using CapaNegocio;
using System.Collections.Generic;

namespace MantenedorWEB
{
    public partial class frmModificarProy : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["idproyecto"] != null)
                {
                    int idproyecto = Convert.ToInt32(Request.QueryString["idproyecto"]);
                    entProyecto obj = negProyecto.BuscarProyecto(idproyecto);
                    txtNombre.Text = obj.Nombre;
                    txtDescripcion.Text = obj.Descripcion;
                    List<entUsuario> lista = negUsuario.ListarUsuariosCompleto();
                    foreach (entUsuario usu in lista)
                    {
                        ListItem item = new ListItem(usu.Apellido + ", " + usu.Nombre, usu.idUsuario.ToString());
                        cmbUsuario.Items.Add(item);
                    }
                    cmbUsuario.SelectedValue = obj.refJefeProyecto.ToString();
                    List<entOrganigrama> listaOrg = negOrganigrama.ListarOrganigrama();
                    foreach (entOrganigrama org in listaOrg)
                    {
                        ListItem item = new ListItem(org.cNombreSector, org.idOrganigramaSector.ToString());
                        cmbArea.Items.Add(item);
                    }
                    cmbArea.SelectedValue = obj.refOrganigramaSectores.ToString();
                }
                else
                {
                    lblError.Text = "Debe seleccionar un proyecto";
                    lblError.Visible = true;
                    Response.Redirect("Pruebas.aspx");
                }
            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            if (txtNombre.Text != "" && txtDescripcion.Text != "")
            {
                entProyecto obj = new entProyecto();
                obj.idProyecto = Convert.ToInt32(Request.QueryString["idproyecto"]);
                obj.Nombre = txtNombre.Text;
                obj.Descripcion = txtDescripcion.Text;
                obj.refJefeProyecto = Convert.ToInt32(cmbUsuario.SelectedValue);
                obj.refOrganigramaSectores = Convert.ToInt32(cmbArea.SelectedValue);
                if (negProyecto.ModificarProyecto(obj) == 1)
                {
                    Response.Redirect("frmPrincipal.aspx");
                }
                else
                {
                    lblError.Text = "No se pudo modificar el proyecto";
                    lblError.Visible = true;
                }
            }
            else
            {
                lblError.Text = "Faltan ingresar campos";
                lblError.Visible = true;
            }

        }
    }
}