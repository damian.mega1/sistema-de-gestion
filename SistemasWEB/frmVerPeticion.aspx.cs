﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using CapaEntidades;
using CapaNegocio;
using System.Collections.Generic;

namespace MantenedorWEB
{
    public partial class frmVerPeticion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["idPeticion"] != null)
                {
                    int idpeticion = Convert.ToInt32(Request.QueryString["idPeticion"]);
                    entPeticion obj = negPeticion.BuscarPeticion(idpeticion);
                    txtAsunto.Text = obj.Asunto;
                    txtAsunto.Enabled = false;
                    txtDescripcion.Text = obj.Descripcion;
                    txtDescripcion.Enabled = false;
                    List<entProyecto> Lista = negProyecto.ListarProyectos();
                    foreach (entProyecto c in Lista)
                    {
                        ListItem li = new ListItem(c.Nombre, c.idProyecto.ToString());
                        cmbProyectos.Items.Add(li);
                    }
                    cmbProyectos.SelectedValue = obj.refProyecto.ToString();
                    cmbProyectos.Enabled = false;

                    List<entTipoPeticion> ListaTP = negTipoPeticion.ListarTiposPeticion();
                    foreach (entTipoPeticion c in ListaTP)
                    {
                        ListItem li = new ListItem(c.Descripcion, c.idTipoPeticion.ToString());
                        cmbTipoPeticion.Items.Add(li);
                    }
                    cmbTipoPeticion.SelectedValue = obj.refTipo.ToString();
                    cmbTipoPeticion.Enabled = false;

                    List<entEstadoPeticion> ListaEP = negEstadoPeticion.ListarEstadosPeticion();
                    foreach (entEstadoPeticion c in ListaEP)
                    {
                        ListItem li = new ListItem(c.Descripcion, c.idEstadoPeticion.ToString());
                        cmbEstadoPeticion.Items.Add(li);
                    }
                    cmbEstadoPeticion.SelectedValue = obj.refEstado.ToString();
                    cmbEstadoPeticion.Enabled = false;

                    List<entPrioridadPeticion> ListaPP = negPrioridadPeticion.ListarPrioridadesPeticion();
                    foreach (entPrioridadPeticion c in ListaPP)
                    {
                        ListItem li = new ListItem(c.Descripcion, c.idPrioridadPeticion.ToString());
                        cmbPrioridadPeticion.Items.Add(li);
                    }
                    cmbPrioridadPeticion.SelectedValue = obj.refPrioridad.ToString();
                    cmbPrioridadPeticion.Enabled = false;

                    List<entUsuario> ListaU = negUsuario.ListarUsuariosCompleto();
                    foreach (entUsuario c in ListaU)
                    {
                        ListItem li = new ListItem(c.Nombre + " " + c.Apellido +"   ", c.idUsuario.ToString());
                        cmbAsignadaA.Items.Add(li);
                    }
                    cmbAsignadaA.SelectedValue = obj.refAsignadaA.ToString();
                    cmbAsignadaA.Enabled = false;

                    txtFechaInicio.Text = obj.FechaInicio.ToShortDateString();
                    txtFechaInicio.Enabled = false;
                    txtFechaFin.Text = obj.FechaFin.ToShortDateString();
                    txtFechaFin.Enabled = false;
                    txtTiempoEstimado.Text = obj.TiempoEstimado.ToString();
                    txtTiempoEstimado.Enabled = false;
                    txtPorcentajeRealizado.Text = obj.PorcentajeRealizado.ToString();
                    txtPorcentajeRealizado.Enabled = false;
                }
                else
                {
                    Response.Redirect("frmPrincipal.aspx");
                }
            }
        }

        protected void btnVolver_Click(object sender, EventArgs e)
        {
            Response.Redirect("Pruebas.aspx");
        }
    }
}
