﻿<%@ Page Language="C#" MasterPageFile="~/Anonimo.Master" AutoEventWireup="true" CodeBehind="frmBuscarUsuarios.aspx.cs" Inherits="MantenedorWEB.frmBuscarUsuarios" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2><br />Usuarios</h2>

    <asp:GridView ID="grvUsuarios" runat="server" AutoGenerateColumns="False" 
        HeaderStyle-CssClass="header" Width="740px" 
        onselectedindexchanged="grvUsuarios_SelectedIndexChanged" 
        OnRowEditing="grvUsuarios_RowEditing" EnableModelValidation="True" BackColor="White" CssClass="mygrdContent" BorderColor="#9aca3c" PagerStyle-CssClass="pager"> 
        <Columns>
            <asp:BoundField DataField="idUsuario" HeaderText="# Usuario" />
            <asp:BoundField DataField="Apellido" HeaderText="Apellido" >
                <ControlStyle Width="80px" />
            </asp:BoundField>
            <asp:BoundField DataField="Nombre" HeaderText="Nombre" />
            <asp:BoundField DataField="Funcion" HeaderText="Función" />
            <asp:BoundField DataField="TipoUsuario" 
                HeaderText="Tipo de usuario" />
            <asp:CommandField ShowSelectButton="True" EditText="Modificar" SelectText="Ver" 
                ShowEditButton="True" />
        </Columns>
        <HeaderStyle />
    </asp:GridView>

</asp:Content>
