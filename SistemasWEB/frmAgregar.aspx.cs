﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using CapaNegocio;
using CapaEntidades;
using System.Collections.Generic;
using System.Net.Mail;
using System.Net;

namespace MantenedorWEB
{
    public partial class frmAgregar : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                List<entProyecto> lista = negProyecto.ListarProyectos();
                foreach (entProyecto obj in lista)
                {
                    ListItem item = new ListItem(obj.Nombre, obj.idProyecto.ToString());
                    cmbProyectos.Items.Add(item);
                }

                List<entTipoPeticion> listaTP = negTipoPeticion.ListarTiposPeticion();
                foreach (entTipoPeticion obj in listaTP)
                {
                    ListItem item = new ListItem(obj.Descripcion, obj.idTipoPeticion.ToString());
                    cmbTipoPeticion.Items.Add(item);
                }

                List<entEstadoPeticion> listaEP = negEstadoPeticion.ListarEstadosPeticion();
                foreach (entEstadoPeticion obj in listaEP)
                {
                    ListItem item = new ListItem(obj.Descripcion, obj.idEstadoPeticion.ToString());
                    cmbEstadoPeticion.Items.Add(item);
                }

                List<entPrioridadPeticion> listaPP = negPrioridadPeticion.ListarPrioridadesPeticion();
                foreach (entPrioridadPeticion obj in listaPP)
                {
                    ListItem item = new ListItem(obj.Descripcion, obj.idPrioridadPeticion.ToString());
                    cmbPrioridadPeticion.Items.Add(item);
                }

                List<entUsuario> listaAa = negUsuario.ListarUsuariosCompleto();
                foreach (entUsuario obj in listaAa)
                {
                    ListItem item = new ListItem(obj.Apellido + ", " + obj.Nombre, obj.idUsuario.ToString());
                    cmbAsignadaA.Items.Add(item);
                }
            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            if (txtDescripcion.Text != "" && txtAsunto.Text != "" )
            {
                entPeticion obj = new entPeticion();
                obj.Asunto = txtAsunto.Text;
                obj.Descripcion = txtDescripcion.Text;
                obj.refProyecto = Convert.ToInt32(cmbProyectos.SelectedValue);
                obj.refTipo = Convert.ToInt32(cmbTipoPeticion.SelectedValue);
                obj.refEstado = Convert.ToInt32(cmbEstadoPeticion.SelectedValue);
                obj.refPrioridad = Convert.ToInt32(cmbPrioridadPeticion.SelectedValue);
                obj.refAsignadaA = Convert.ToInt32(cmbAsignadaA.SelectedValue);
                obj.FechaInicio = calFechaInicio.SelectedDate;
                obj.FechaFin = calFechaFin.SelectedDate;

                try
                {
                    obj.TiempoEstimado = float.Parse(txtTiempoEstimado.Text);
                    obj.PorcentajeRealizado = Convert.ToInt32(txtPorcentajeRealizado.Text);
                }
                catch (Exception ex)
                {
                    lblError.Text = "Tiempo estimado o Porcentaje realizado: Datos invalidos";
                    lblError.Visible = true;
                    return;
                }


                if (negPeticion.AgregarPeticion(obj) == 1)
                {
                    Response.Redirect("frmAgregar.aspx");
                }
                else
                {
                    lblError.Text = "No se pudo agregar la petición";
                    lblError.Visible = true;
                }
            }
            else
            {
                lblError.Text = "Faltan ingresar campos";
                lblError.Visible = true;
            }
        }


        protected void btnGuardarySalir_Click(object sender, EventArgs e)
        {
           



            if (txtDescripcion.Text != "" && txtAsunto.Text != "")
            {
                entPeticion obj = new entPeticion();
                obj.Asunto = txtAsunto.Text;
                obj.Descripcion = txtDescripcion.Text;
                obj.refProyecto = Convert.ToInt32(cmbProyectos.SelectedValue);
                obj.refTipo = Convert.ToInt32(cmbTipoPeticion.SelectedValue);
                obj.refEstado = Convert.ToInt32(cmbEstadoPeticion.SelectedValue);
                obj.refPrioridad = Convert.ToInt32(cmbPrioridadPeticion.SelectedValue);
                obj.refAsignadaA = Convert.ToInt32(cmbAsignadaA.SelectedValue);
                obj.FechaInicio = calFechaInicio.SelectedDate;
                obj.FechaFin = calFechaFin.SelectedDate;

                try
                {
                    obj.TiempoEstimado = float.Parse(txtTiempoEstimado.Text);
                    obj.PorcentajeRealizado = Convert.ToInt32(txtPorcentajeRealizado.Text);
                }
                catch (Exception ex)
                {
                    lblError.Text = "Tiempo estimado o Porcentaje realizado: Datos invalidos";
                    lblError.Visible = true;
                    return;
                }


                if (negPeticion.AgregarPeticion(obj) == 1)
                {
                    Response.Redirect("Pruebas.aspx");
                }
                else
                {
                    lblError.Text = "No se pudo agregar la petición";
                    lblError.Visible = true;
                }
            }
            else
            {
                lblError.Text = "Faltan ingresar campos";
                lblError.Visible = true;
            }
        }

        protected void btnVolver_Click(object sender, EventArgs e)
        {
            Response.Redirect("Pruebas.aspx");
        }


        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            if (calFechaInicio.Visible)
            {
                calFechaInicio.Visible = false;
            }
            else
            {
                calFechaInicio.Visible = true;
            }
         }

         protected void calFechaInicio_SelectionChanged(object sender, EventArgs e)
         {
            txtFechaInicio.Text=calFechaInicio.SelectedDate.ToShortDateString();
            calFechaInicio.Visible=false;
         }

         protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
         {
             if (calFechaFin.Visible)
             {
                 calFechaFin.Visible = false;
             }
             else
             {
                 calFechaFin.Visible = true;
             }
         }

         protected void calFechaFin_SelectionChanged(object sender, EventArgs e)
         {
             txtFechaFin.Text = calFechaFin.SelectedDate.ToShortDateString();
             calFechaFin.Visible = false;
         }
    }
}

