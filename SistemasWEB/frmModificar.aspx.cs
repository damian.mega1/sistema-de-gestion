﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using CapaEntidades;
using CapaNegocio;
using System.Collections.Generic;

namespace MantenedorWEB
{
    public partial class frmModificar : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["idpeticion"] != null)
                {
                    int idpeticion = Convert.ToInt32(Request.QueryString["idpeticion"]);
                    entPeticion obj = negPeticion.BuscarPeticion(idpeticion);
                    txtAsunto.Text = obj.Asunto;
                    txtDescripcion.Text = obj.Descripcion;
                    List<entProyecto> Lista = negProyecto.ListarProyectos();
                    foreach (entProyecto c in Lista)
                    {
                        ListItem li = new ListItem(c.Nombre, c.idProyecto.ToString());
                        cmbProyectos.Items.Add(li);
                    }
                    cmbProyectos.SelectedValue = obj.refProyecto.ToString();

                    List<entTipoPeticion> ListaTP = negTipoPeticion.ListarTiposPeticion();
                    foreach (entTipoPeticion c in ListaTP)
                    {
                        ListItem li = new ListItem(c.Descripcion, c.idTipoPeticion.ToString());
                        cmbTipoPeticion.Items.Add(li);
                    }
                    cmbTipoPeticion.SelectedValue = obj.refTipo.ToString();

                    List<entEstadoPeticion> ListaEP = negEstadoPeticion.ListarEstadosPeticion();
                    foreach (entEstadoPeticion c in ListaEP)
                    {
                        ListItem li = new ListItem(c.Descripcion, c.idEstadoPeticion.ToString());
                        cmbEstadoPeticion.Items.Add(li);
                    }
                    cmbEstadoPeticion.SelectedValue = obj.refEstado.ToString();

                    List<entPrioridadPeticion> ListaPP = negPrioridadPeticion.ListarPrioridadesPeticion();
                    foreach (entPrioridadPeticion c in ListaPP)
                    {
                        ListItem li = new ListItem(c.Descripcion, c.idPrioridadPeticion.ToString());
                        cmbPrioridadPeticion.Items.Add(li);
                    }
                    cmbPrioridadPeticion.SelectedValue = obj.refPrioridad.ToString();

                    List<entUsuario> ListaU = negUsuario.ListarUsuariosCompleto();
                    foreach (entUsuario c in ListaU)
                    {
                        ListItem li = new ListItem(c.Nombre + " " + c.Apellido + "   ", c.idUsuario.ToString());
                        cmbAsignadaA.Items.Add(li);
                    }
                    cmbAsignadaA.SelectedValue = obj.refAsignadaA.ToString();

                    calFechaInicio.SelectedDate = obj.FechaInicio;
                    txtFechaInicio.Text = obj.FechaInicio.ToShortDateString();
                    calFechaFin.SelectedDate = obj.FechaFin;
                    txtFechaFin.Text = obj.FechaFin.ToShortDateString();
                    txtTiempoEstimado.Text = obj.TiempoEstimado.ToString();
                    txtPorcentajeRealizado.Text = obj.PorcentajeRealizado.ToString();
                }
                else
                {
                    Response.Redirect("Pruebas.aspx");
                }
            }
        }

        protected void btnModificar_Click(object sender, EventArgs e)
        {
            if (txtAsunto.Text != "" && txtDescripcion.Text != "")
            {
                entPeticion obj = new entPeticion();
                obj.idPeticion = Convert.ToInt32(Request.QueryString["idpeticion"]);
                obj.refProyecto = Convert.ToInt32(cmbProyectos.SelectedValue);
                obj.refTipo = Convert.ToInt32(cmbTipoPeticion.SelectedValue);
                obj.Asunto = txtAsunto.Text;
                obj.Descripcion = txtDescripcion.Text;
                obj.refEstado = Convert.ToInt32(cmbEstadoPeticion.SelectedValue);
                obj.refPrioridad = Convert.ToInt32(cmbPrioridadPeticion.SelectedValue);
                obj.refAsignadaA = Convert.ToInt32(cmbAsignadaA.SelectedValue);
                obj.FechaInicio = Convert.ToDateTime(calFechaInicio.SelectedDate);
                obj.FechaFin = Convert.ToDateTime(calFechaFin.SelectedDate);
                obj.TiempoEstimado = float.Parse(txtTiempoEstimado.Text);
                obj.PorcentajeRealizado = Convert.ToInt32(txtPorcentajeRealizado.Text);
                if (negPeticion.ModificarPeticion(obj) == 1)
                {
                    CCorreo objCorreo = new CCorreo(GlobalVariables.GlobalMailA,"Modificacion", "Se a Modificado una Peticion de su Proyecto");
                    if (objCorreo.Estado)
                    {
                        GlobalVariables.GlobalMsgMail = "El correo se envió correctamente";
                    }
                    else
                    {
                        GlobalVariables.GlobalMsgMail = "Error al enviar el correo, verifique ..<br>" + objCorreo.mensaje_error;
                    }
                

                    Response.Redirect("Pruebas.aspx");
                }


                else
                {
                    lblError.Text = "No se pudo modificar la petición";
                    lblError.Visible = true;
                }
            }
            else
            {
                lblError.Text = "Faltan ingresar campos";
                lblError.Visible = true;
            }

        }

        protected void btnVolver_Click(object sender, EventArgs e)
        {
            Response.Redirect("Pruebas.aspx");
        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            if (calFechaInicio.Visible)
            {
                calFechaInicio.Visible = false;
            }
            else
            {
                calFechaInicio.Visible = true;
            }
        }

        protected void calFechaInicio_SelectionChanged(object sender, EventArgs e)
        {
            txtFechaInicio.Text = calFechaInicio.SelectedDate.ToShortDateString();
            calFechaInicio.Visible = false;
        }

        protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
        {
            if (calFechaFin.Visible)
            {
                calFechaFin.Visible = false;
            }
            else
            {
                calFechaFin.Visible = true;
            }
        }

        protected void calFechaFin_SelectionChanged(object sender, EventArgs e)
        {
            txtFechaFin.Text = calFechaFin.SelectedDate.ToShortDateString();
            calFechaFin.Visible = false;
        }
    }
}
