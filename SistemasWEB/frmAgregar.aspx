﻿<%@ Page Language="C#" MasterPageFile="~/Anonimo.Master" AutoEventWireup="true" CodeBehind="frmAgregar.aspx.cs" Inherits="MantenedorWEB.frmAgregar" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div id="abrigo_formulario">
    <h2><br />   Nueva petición</h2>
        <table>
        <tr>    
            <td class="thick">Proyecto: (*) </td>
            <td>
                <asp:DropDownList ID="cmbProyectos" runat="server" Width ="200px" CssClass="mydropdownlist1" TextMode="MultiLine">
                <asp:ListItem>Seleccionar</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>    
            <td class="thick">Tipo de petición: (*) </td>
            <td>
                <asp:DropDownList ID="cmbTipoPeticion" runat="server" Width ="200px" CssClass="mydropdownlist1">
                <asp:ListItem>Seleccionar</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="thick">Asunto: (*) </td>
            <td>
                <asp:TextBox ID="txtAsunto" runat="server" Width ="430px" CssClass="mydropdownlist1"></asp:TextBox>
            </td>
        </tr>
        <tr>    
            <td class="thick">Descripción: (*)</td>
            <td>
               <asp:TextBox ID="txtDescripcion" runat="server" Width ="430px" Height = "90px" CssClass="mydropdownlist1" 
                    TextMode="MultiLine"></asp:TextBox>
            </td>
        </tr>
        <tr>    
            <td class="thick">Estado peticion: (*) </td>
            <td>
                <asp:DropDownList ID="cmbEstadoPeticion" runat="server" Width ="200px" CssClass="mydropdownlist1">
                <asp:ListItem>Seleccionar</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>    
            <td class="thick">Prioridad: (*) </td>
            <td>
                <asp:DropDownList ID="cmbPrioridadPeticion" runat="server" Width ="200px" CssClass="mydropdownlist1">
                <asp:ListItem>Seleccionar</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>    
            <td class="thick">Asignada a: (*) </td>
            <td>
                <asp:DropDownList ID="cmbAsignadaA" runat="server" Width ="200px" CssClass="mydropdownlist1">
                <asp:ListItem>Seleccionar</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>    
            <td class="thick">Fecha de inicio: (*) </td>
            <td>
                <asp:TextBox ID="txtFechaInicio" runat="server" Width ="200px" CssClass="mydropdownlist1"></asp:TextBox>
                <asp:ImageButton ID="ImageButton1" runat="server" onclick="ImageButton1_Click" 
                    Height="21px" ImageUrl="~/Imagenes/calen2.png" Width="55px" />
                <asp:Calendar ID="calFechaInicio" runat="server" Width ="350px" Visible="False" 
                    onselectionchanged="calFechaInicio_SelectionChanged" BackColor="White" BorderColor="White" BorderWidth="1px" Font-Names="Verdana" Font-Size="9pt" ForeColor="Black" Height="190px" NextPrevFormat="FullMonth">
                    <DayHeaderStyle Font-Bold="True" Font-Size="8pt" />
                    <NextPrevStyle Font-Bold="True" Font-Size="8pt" ForeColor="#333333" VerticalAlign="Bottom" />
                    <OtherMonthDayStyle ForeColor="#999999" />
                    <SelectedDayStyle BackColor="#333399" ForeColor="White" />
                    <TitleStyle BackColor="White" BorderColor="Black" BorderWidth="4px" Font-Bold="True" Font-Size="12pt" ForeColor="#333399" />
                    <TodayDayStyle BackColor="#CCCCCC" />
                </asp:Calendar>
            </td>
        </tr>
        <tr>    
            <td class="thick">Fecha de finalización: (*) </td>
            <td>
                <asp:TextBox ID="txtFechaFin" runat="server" Width ="200px" CssClass="mydropdownlist1"></asp:TextBox>
                <asp:ImageButton ID="ImageButton2" runat="server" onclick="ImageButton2_Click" 
                    Height="19px" ImageUrl="~/Imagenes/calen2.png" Width="53px"/>
                <asp:Calendar ID="calFechaFin" runat="server" Width ="350px" Visible="False" 
                    onselectionchanged="calFechaFin_SelectionChanged" BackColor="White" BorderColor="White" BorderWidth="1px" Font-Names="Verdana" Font-Size="9pt" ForeColor="Black" Height="190px" NextPrevFormat="FullMonth" >
                    <DayHeaderStyle Font-Bold="True" Font-Size="8pt" />
                    <NextPrevStyle Font-Bold="True" Font-Size="8pt" ForeColor="#333333" VerticalAlign="Bottom" />
                    <OtherMonthDayStyle ForeColor="#999999" />
                    <SelectedDayStyle BackColor="#333399" ForeColor="White" />
                    <TitleStyle BackColor="White" BorderColor="Black" BorderWidth="4px" Font-Bold="True" Font-Size="12pt" ForeColor="#333399" />
                    <TodayDayStyle BackColor="#CCCCCC" />
                </asp:Calendar>
            </td>
        </tr>
        <tr>    
            <td class="thick">Tiempo Estimado (días):(*)</td>
            <td>
               <asp:TextBox ID="txtTiempoEstimado" runat="server" Width ="200px" CssClass="mydropdownlist1" ></asp:TextBox>
            </td>
        </tr>
        <tr>    
            <td class="thick">% Realizado: (*) </td>
            <td>
               <asp:TextBox ID="txtPorcentajeRealizado" runat="server" Width ="200px" CssClass="mydropdownlist1"></asp:TextBox>
            </td>
        </tr>
       <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblError" runat="server" Text="" Visible="false" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center"><asp:Button ID="btnGuardar" runat="server" CssClass="boton-gral" 
                    Text="Guardar y Continuar" Width="170px" onclick="btnGuardar_Click"/>
                    <asp:Button ID="btnGuardarySalir" runat="server" CssClass="boton-gral" 
                    Text="Guardar y Salir" Width="170px" onclick="btnGuardarySalir_Click"/>
                    <asp:Button ID="btnVolver" runat="server" CssClass="boton-gral" 
                    Text="Volver" Width="170px" onclick="btnVolver_Click"/>
                    <p class="p"> (*) Los Campos Son Obligatorios </p>
            </td>
        
        </tr>
        
            
        </table>
    </div>

</asp:Content>
