﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;

namespace MantenedorWEB
{
    public partial class frmReporte : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            //generar reporte con itextsharp
            Response.ContentType = "aplication / pdf";
            Response.AddHeader("content-disposition", "attachment; filename = reporte.pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);

            StringWriter StringWriter = new StringWriter();
            HtmlTextWriter HtmlTextWriter = new HtmlTextWriter(StringWriter);

            GridView1.DataBind();
            GridView1.RenderControl(HtmlTextWriter);
            GridView1.HeaderRow.Style.Add("ancho", "10%");
            GridView1.HeaderRow.Style.Add("font-size", "15px");
            GridView1.Style.Add("text-decoration", "ninguno");
            GridView1.Style.Add("font-family", "Arial, helvetica, sans-serif;");
            GridView1.Style.Add("font-size", "40px");
            

            
            //StreamReader sr = new StreamReader (StringWriter.ToString ());
            StringReader sr = new StringReader(StringWriter.ToString());
            Document doc = new Document(PageSize.A2, 7F, 7F, 7F, 0f);
            HTMLWorker HTMLParser = new HTMLWorker (doc);
            PdfWriter.GetInstance(doc, Response.OutputStream);
            doc.Open();
            HTMLParser.Parse(sr);
            doc.Close();
            Response.Write(doc);
            Response.End();

        }
        public override void VerifyRenderingInServerForm(Control control)
        {
            
        }
    }
}