﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using CapaEntidades;
using CapaNegocio;
using System.Collections.Generic;

namespace MantenedorWEB
{
    public partial class frmAgregarProy : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                List<entUsuario> lista = negUsuario.ListarUsuariosCompleto();
                foreach (entUsuario obj in lista)
                {
                    ListItem item = new ListItem(obj.Apellido + ", " + obj.Nombre, obj.idUsuario.ToString());
                    cmbUsuario.Items.Add(item);
                }
                List<entOrganigrama> listaOrg = negOrganigrama.ListarOrganigrama();
                foreach (entOrganigrama obj in listaOrg)
                {
                    ListItem item = new ListItem(obj.cNombreSector, obj.idOrganigramaSector.ToString());
                    cmbArea.Items.Add(item);
                }
            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            if (txtNombre.Text != "" && txtDescripcion.Text != "")
            {
                entProyecto obj = new entProyecto();
                obj.Nombre = txtNombre.Text;
                obj.Descripcion = txtDescripcion.Text;
                obj.refJefeProyecto = Convert.ToInt32(cmbUsuario.SelectedValue);
                obj.refOrganigramaSectores = Convert.ToInt32(cmbArea.SelectedValue);
                if (negProyecto.AgregarProyecto(obj) == 1)
                {
                    Response.Redirect("frmAgregarProy.aspx");
                }
                else
                {
                    lblError.Text = "No se pudo agregar";
                    lblError.Visible = true;
                }
            }
            else
            {
                lblError.Text = "Faltan ingresar campos";
                lblError.Visible = true;
            }
        }

        protected void btnGuardarySalir_Click(object sender, EventArgs e)
        {
            if (txtNombre.Text != "" && txtDescripcion.Text != "")
            {
                entProyecto obj = new entProyecto();
                obj.Nombre = txtNombre.Text;
                obj.Descripcion = txtDescripcion.Text;
                obj.refJefeProyecto = Convert.ToInt32(cmbUsuario.SelectedValue);
                obj.refOrganigramaSectores = Convert.ToInt32(cmbArea.SelectedValue);
                if (negProyecto.AgregarProyecto(obj) == 1)
                {
                    Response.Redirect("Pruebas.aspx");
                }
                else
                {
                    lblError.Text = "No se pudo agregar";
                    lblError.Visible = true;
                }
            }
            else
            {
                lblError.Text = "Faltan ingresar campos";
                lblError.Visible = true;
            }
        }

        protected void btnVolver_Click(object sender, EventArgs e)
        {
            Response.Redirect("Pruebas.aspx");
        }

    }
}
