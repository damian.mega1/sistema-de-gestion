﻿<%@ Page Language="C#" MasterPageFile="~/Anonimo.Master" AutoEventWireup="true" CodeBehind="frmModificarProy.aspx.cs" Inherits="MantenedorWEB.frmModificarProy" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div id="abrigo_formulario">
    <h2><br />Modificar proyecto</h2>
    <br />
        <table>
        <tr>
            <td class="thick">Nombre: </td>
            <td>
                <asp:TextBox ID="txtNombre" runat="server" Width ="430px" CssClass="mydropdownlist1"></asp:TextBox>
            </td>
        </tr>
        <tr>    
            <td class="thick">Descripción: </td>
            <td>
               <asp:TextBox ID="txtDescripcion" runat="server" Width ="430px" Height = "90px" CssClass="mydropdownlist1"
                    TextMode="MultiLine"></asp:TextBox>
            </td>
        </tr>
        <tr>    
            <td class="thick">Jefe de proyecto: </td>
            <td>
                <asp:DropDownList ID="cmbUsuario" runat="server" Width ="200px" CssClass="mydropdownlist1">
                <asp:ListItem>Seleccionar</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>    
            <td class="thick">Area del proyecto: </td>
            <td>
               <asp:DropDownList ID="cmbArea" runat="server" Width ="200px" CssClass="mydropdownlist1">
               <asp:ListItem>Seleccionar</asp:ListItem>
               </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblError" runat="server" Text="" Visible="false" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
            <br />
                <asp:Button ID="btnGuardar" runat="server" CssClass="boton-gral"
                    Text="Guardar" Width="170px" onclick="btnGuardar_Click" /></td>
        </tr>    
        </table>
    </div>

</asp:Content>
