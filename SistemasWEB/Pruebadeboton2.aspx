﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Pruebadeboton2.aspx.cs" Inherits="MantenedorWEB.Pruebadeboton2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        .btn-primary
        {
            width: 90px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <button type="button" class="btn btn-primary">Destacado</button>
        <asp:Button ID="btnAceptar" class="btn-primary" runat="server" OnClick="btnAceptar_Click" Text="Button" />
        <br />
        <br />
        <asp:TextBox ID="txtUsuario" runat="server"></asp:TextBox>
        <br />
        <br />
        <asp:TextBox ID="txtPass" runat="server"></asp:TextBox>
        <br />
        <br />
        <asp:Label ID="lblError" runat="server" ForeColor="Red" Visible="False"></asp:Label>
        <br />
        <br />
    </div>
    </form>
</body>
</html>
