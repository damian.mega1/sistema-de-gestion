﻿<%@ Page Language="C#"MasterPageFile="~/Anonimo.Master" AutoEventWireup="true" CodeBehind="frmReporte.aspx.cs" Inherits="MantenedorWEB.frmReporte" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div id="abrigo_formulario">
    <div id="contenido_derecho_reporte">

        &nbsp;

        <br />
        <!--<asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Descargar Reporte" Width="133px" />-->
        <asp:ImageButton ID="Reporte" runat="server" ImageUrl="Imagenes/file_pdf_download.png" OnClick="Button1_Click" style="margin-left: 51px" /> 
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <br />
        <br />
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" EnableModelValidation="True" BackColor="White" BorderColor="#3366CC" BorderStyle="None" BorderWidth="1px" CellPadding="4" style="margin-right: 137px" Width="656px">
            <Columns>
                <asp:BoundField DataField="Nombre" HeaderText="Proyecto" SortExpression="Nombre" >
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                </asp:BoundField>
                <asp:BoundField DataField="Asunto" HeaderText="Asunto" SortExpression="Asunto" >                
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                </asp:BoundField>
                <asp:BoundField DataField="Expr1" HeaderText="Peticion" SortExpression="Expr1" >
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                </asp:BoundField>
                <asp:BoundField DataField="Descripcion" HeaderText="Descripcion" SortExpression="Descripcion" >
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                </asp:BoundField>
                <asp:BoundField DataField="FechaInicio" DataFormatString="{0:d}" HeaderText="Fecha Inicio" SortExpression="FechaInicio" >
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                </asp:BoundField>
                <asp:BoundField DataField="FechaFin" DataFormatString="{0:d}" HeaderText="Fecha Fin" SortExpression="FechaFin">
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                </asp:BoundField>
                <asp:BoundField DataField="TiempoEstimado" HeaderText="Tiempo Estimado (dias)" SortExpression="TiempoEstimado" >
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="PorcentajeRealizado" HeaderText="Porcentaje Realizado (%)" SortExpression="PorcentajeRealizado" >
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="Expr2" HeaderText="Estado" SortExpression="Expr2" >
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                </asp:BoundField>
            </Columns>
            <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
            <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF" />
            <PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" />
            <RowStyle BackColor="White" ForeColor="#003399" />
            <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
        </asp:GridView>
        <br />
        <br />
        <!--<asp:BoundField DataField="OrderDate" DataFormatString="{0:d}" HeaderText="OrderDate" SortExpression="OrderDate" />-->
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="Data Source=192.168.1.19;Initial Catalog=MIVSP_SisControl;User ID=svicente;Password=ciganoseu7" SelectCommand="SELECT Proyecto.Nombre, Proyecto.Descripcion, Peticion.Asunto, Peticion.Descripcion AS Expr1, Peticion.FechaInicio, Peticion.FechaFin, Peticion.TiempoEstimado, Peticion.PorcentajeRealizado, EstadoPeticion.Descripcion AS Expr2 FROM Peticion INNER JOIN Proyecto ON Peticion.refProyecto = Proyecto.idProyecto INNER JOIN EstadoPeticion ON Peticion.refEstado = EstadoPeticion.idEstado ORDER BY Proyecto.Nombre" ProviderName="<%$ ConnectionStrings:MIVSP_SisControlConnectionString6.ProviderName %>"></asp:SqlDataSource>

    </div>
    </div>
</asp:Content>
 
