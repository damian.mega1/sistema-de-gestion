﻿<%@ Page Language="C#" MasterPageFile="~/Anonimo.Master" AutoEventWireup="true" CodeBehind="frmVerUsuario.aspx.cs" Inherits="MantenedorWEB.frmVerUsuario" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="abrigo_formulario">
    <h2><br />Ver Usuario</h2>
        <table>
        <tr>    
            <td class="thick">Apellido: </td>
            <td>
                <asp:TextBox ID="txtApellido" runat="server" Width ="230px"  Enabled="false" CssClass="mydropdownlist1"></asp:TextBox>
            </td>
        </tr>
        <tr>    
            <td class="thick">Nombres: </td>
            <td>
                <asp:TextBox ID="txtNombre" runat="server" Width ="230px" Enabled="false" CssClass="mydropdownlist1"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="thick">Usuario: </td>
            <td>
                <asp:TextBox ID="txtUsuario" runat="server" Width ="230px" Enabled="false" CssClass="mydropdownlist1"></asp:TextBox>
            </td>
        </tr>
        <tr>    
            <td class="thick">Password: </td>
            <td>
               <asp:TextBox ID="txtPassword" runat="server" Width ="230px" Enabled="false" CssClass="mydropdownlist1"></asp:TextBox>
            </td>
        </tr>
        <tr>    
            <td class="thick">email: </td>
            <td>
                <asp:TextBox ID="txtEmail" runat="server" Width ="230px" Enabled="false" CssClass="mydropdownlist1"></asp:TextBox>
            </td>
        </tr>
        <tr>    
            <td class="thick">Función: </td>
            <td>
                <asp:DropDownList ID="cmbFuncion" runat="server" Width ="230px" Enabled="false" CssClass="mydropdownlist1">
                <asp:ListItem>Seleccionar</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>    
            <td class="thick">Tipo de Usuario: </td>
            <td>
                <asp:DropDownList ID="cmbTipoUsuario" runat="server" Width ="230px" Enabled="false" CssClass="mydropdownlist1">
                <asp:ListItem>Seleccionar</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:Label ID="lblError" runat="server" Text="" Visible="false" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                <br />
                    <asp:Button ID="btnVolver" runat="server" CssClass="boton-gral"
                        Text="Volver" Width="170px" OnClick="btnVolver_Click" />
                </td>
            </tr>
        </table>
    </div>

</asp:Content>
