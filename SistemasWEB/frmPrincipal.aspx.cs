﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using CapaNegocio;
using CapaEntidades;
using System.Collections.Generic;

namespace MantenedorWEB
{
    public partial class frmPrincipal : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(Request.QueryString["cat"]!= null)
            {
                int cat = Convert.ToInt32(Request.QueryString["cat"]);
                grvPeticiones.DataSource = negPeticion.ListarPeticionesPorProyecto(cat);
                grvPeticiones.DataBind();
                //btnAgregar.Visible = true;
                lblTitulo.Visible = true;
                lblNombre.Visible = true;
                txtNombre.Text = negProyecto.BuscarProyecto(cat).Nombre;
                txtNombre.Visible = true;
                lblDescripcion.Visible = true;
                txtDescripcion.Text = negProyecto.BuscarProyecto(cat).Descripcion;
                txtDescripcion.Visible = true;
                lblJefe.Visible = true;
                lblArea.Visible = true;
                int refjefe = negProyecto.BuscarProyecto(cat).refJefeProyecto;
                txtJefeProyecto.Text = negUsuario.BuscarUsuario(refjefe).Apellido + ", " +
                                        negUsuario.BuscarUsuario(refjefe).Nombre;
                txtJefeProyecto.Visible = true;
                int refArea = negProyecto.BuscarProyecto(cat).refOrganigramaSectores;
                txtAreaProyecto.Text = negOrganigrama.BuscarOrganigrama(refArea).cNombreSector;
                txtAreaProyecto.Visible = true;
            }
        }

        void ValueHiddenField_ValueChanged(Object sender, EventArgs e)
        {

            // Display the value of the HiddenField control.
            //Message.Text = "The value of the HiddenField control is " + ValueHiddenField.Value + ".";

        }

        

        //protected void btnAgregar_Click(object sender, EventArgs e)
        //{
        //    Response.Redirect("frmAgregar.aspx");
        //}

        protected void btnVolver_Click(object sender, EventArgs e)
        {
            Response.Redirect("Pruebas.aspx");
        }


        protected void grvPeticiones_SelectedIndexChanged(object sender, EventArgs e)
        {
            ///string bot = grvPeticiones.Rows[1].Cells[3].Text;
            ///string peticion = grvPeticiones.Rows[grvPeticiones.SelectedRow.DataItemIndex].Cells[0].Text;
            ///string boton = grvPeticiones.Rows[grvPeticiones.SelectedRow.DataItemIndex].Cells[5].Controls.Count.ToString();
            //string boton2 = grvPeticiones.Rows[grvPeticiones.SelectedRow.DataItemIndex].Cells[5].Controls
            //int idpeticion2 = (e as GridViewEditEventArgs).NewEditIndex;

            //string peticion2 = grvPeticiones.Rows[grvPeticiones.SelectedRow].ClientID;
            ///int Col = (sender as GridView).SelectedIndex;
            Response.Redirect("frmVerPeticion.aspx?idPeticion=" + grvPeticiones.Rows[grvPeticiones.SelectedIndex].Cells[0].Text);
        }

      ///  protected void grvPeticiones_SelectedIndexChanged(object sender, EventArgs e)
      //  {
      //      int Columna = (sender as GridView).SelectedIndex;
      //      string bot = grvPeticiones.Rows[1].Cells[3].Text;
      //      int celda = grvPeticiones.Rows[grvPeticiones.SelectedIndex].DataItemIndex;
      //      TableCell cell = grvPeticiones.Rows[grvPeticiones.SelectedIndex].Cells[3];
      //      TableCell cell2 = new TableCell();
      //      int celda2 = grvPeticiones.Rows[grvPeticiones.SelectedIndex].Cells.GetCellIndex(cell);
      //      int celda3 = grvPeticiones.Rows[grvPeticiones.SelectedIndex].Cells.GetCellIndex(cell2);


            //int celda = grvPeticiones.Rows[grvPeticiones.SelectedIndex].Cells.GetCellIndex();
      //     int Col = (sender as GridView).SelectedIndex;
            
            //if ((sender as GridView).Rows.Count = 4)
            //{
     //           Response.Redirect("frmModificar.aspx?idPeticion=" + grvPeticiones.Rows[grvPeticiones.SelectedIndex].Cells[0].Text);
            //}
            //else
            //{
            //    Response.Redirect("frmVerPeticion.aspx?idPeticion=" + grvPeticiones.Rows[grvPeticiones.SelectedIndex].Cells[0].Text);
            //}
     //   }

        protected void grvPeticiones_RowEditing(object sender, EventArgs e)
        {
            ///string bot = grvPeticiones.Rows[1].Cells[3].Text;
            //int linea = grvPeticiones.Rows[grvPeticiones.SelectedIndex].DataItemIndex;
            ///int idpeticion = (e as GridViewEditEventArgs).NewEditIndex;
            ///string idpeticion2 = grvPeticiones.Rows[(e as GridViewEditEventArgs).NewEditIndex].Cells[0].Text;
            //string idpeticion = grvPeticiones.Rows[e..NewEditIndex].Cells[0].Text;
            //string peticion = grvPeticiones.Rows[grvPeticiones.SelectedRow.DataItemIndex].Cells[0].Text;
            //String country = CustomersGridView.Rows[e.NewEditIndex].Cells[6].Text;
            //grvPeticiones.RowCommand();
            //string boton = grvPeticiones.Columns[5];
            //int Col = (sender as GridView).SelectedIndex;
            //Response.Redirect("frmVerPeticion.aspx?idPeticion=" + grvPeticiones.Rows[grvPeticiones.SelectedRow.DataItemIndex].Cells[0].Text);
            Response.Redirect("frmModificar.aspx?idPeticion=" + grvPeticiones.Rows[(e as GridViewEditEventArgs).NewEditIndex].Cells[0].Text);
        }

    
    }
}
