﻿<%@ Page Language="C#" MasterPageFile="~/Anonimo.Master" AutoEventWireup="true" CodeBehind="frmModificar.aspx.cs" Inherits="MantenedorWEB.frmModificar" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<div id="abrigo_formulario">
    <h2><br />Modificar petición</h2>
    <br />
        <table>
        <tr>    
            <td class="thick">Proyecto: </td>
            <td>
                <asp:DropDownList ID="cmbProyectos" runat="server" Width ="200px" CssClass="mydropdownlist1"></asp:DropDownList>
            </td>
        </tr>
        <tr>    
            <td class="thick">Tipo de petición: </td>
            <td>
                <asp:DropDownList ID="cmbTipoPeticion" runat="server" Width ="200px" CssClass="mydropdownlist1"></asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="thick">Asunto: </td>
            <td>
                <asp:TextBox ID="txtAsunto" runat="server" Width ="430px" CssClass="mydropdownlist1"></asp:TextBox>
            </td>
        </tr>
        <tr>    
            <td class="thick">Descripción: </td>
            <td>
               <asp:TextBox ID="txtDescripcion" runat="server" Width ="430px" Height = "90px" CssClass="mydropdownlist1"
                    TextMode="MultiLine"></asp:TextBox>
            </td>
        </tr>
        <tr>    
            <td class="thick">Estado peticion: </td>
            <td>
                <asp:DropDownList ID="cmbEstadoPeticion" runat="server" Width ="200px" CssClass="mydropdownlist1"></asp:DropDownList>
            </td>
        </tr>
        <tr>    
            <td class="thick">Prioridad: </td>
            <td>
                <asp:DropDownList ID="cmbPrioridadPeticion" runat="server" Width ="200px" CssClass="mydropdownlist1"></asp:DropDownList>
            </td>
        </tr>
        <tr>    
            <td class="thick">Asignada a: </td>
            <td>
                <asp:DropDownList ID="cmbAsignadaA" runat="server" Width ="200px" CssClass="mydropdownlist1"></asp:DropDownList>
            </td>
        </tr>
        <tr>    
            <td class="thick">Fecha de inicio: </td>
            <td>
                <asp:TextBox ID="txtFechaInicio" runat="server" Width ="200px" CssClass="mydropdownlist1"></asp:TextBox>
                <asp:ImageButton ID="ImageButton1" runat="server" onclick="ImageButton1_Click" 
                    Height="26px" ImageUrl="~/Imagenes/calen.png" />
                <asp:Calendar ID="calFechaInicio" runat="server" Width ="200px" Visible="false" 
                    onselectionchanged="calFechaInicio_SelectionChanged"></asp:Calendar>
            </td>
        </tr>
        <tr>    
            <td class="thick">Fecha de finalización: </td>
            <td>
                <asp:TextBox ID="txtFechaFin" runat="server" Width ="200px" CssClass="mydropdownlist1"></asp:TextBox>
                <asp:ImageButton ID="ImageButton2" runat="server" onclick="ImageButton2_Click" 
                    Height="24px" ImageUrl="~/Imagenes/calen.png"/>
                <asp:Calendar ID="calFechaFin" runat="server" Width ="200px" Visible="false" 
                    onselectionchanged="calFechaFin_SelectionChanged" ></asp:Calendar>
            </td>
        </tr>
        <tr>    
            <td class="thick">Tiempo Estimado (días): </td>
            <td>
               <asp:TextBox ID="txtTiempoEstimado" runat="server" Width ="200px" CssClass="mydropdownlist1"></asp:TextBox>
            </td>
        </tr>
        <tr>    
            <td class="thick">% Realizado: </td>
            <td>
               <asp:TextBox ID="txtPorcentajeRealizado" runat="server" Width ="200px" CssClass="mydropdownlist1"></asp:TextBox>
            </td>
        </tr>
       <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblError" runat="server" Text="" Visible="false" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
            <br />
                <asp:Button ID="btnModificar" runat="server" CssClass="boton-gral"
                    Text="Modificar" Width="170px" onclick="btnModificar_Click" />
                    <asp:Button ID="btnVolver" runat="server" CssClass="boton-gral" 
                    Text="Volver" Width="170px" onclick="btnVolver_Click"/>
            </td>
        </tr>    
        </table>

</div>

</asp:Content>
