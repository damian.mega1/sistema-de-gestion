﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Anonimo.Master" AutoEventWireup="true" CodeBehind="frmTablero.aspx.cs" Inherits="MantenedorWEB.frmTablero" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">



     <html >
    <head>


        <!-- -->

        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
        <script type="text/javascript" src="https://code.highcharts.com/highcharts.js"></script>



<script type="text/javascript" src="https://code.highcharts.com/modules/exporting.js"></script>
<script type="text/javascript" src="https://code.highcharts.com/modules/export-data.js"></script>
        
        <!-- -->
        <!--Load the AJAX API-->

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">

 


        //------------------------------

        function volver() {
         

            }


        function Carga() {

      
   

        var colors = Highcharts.getOptions().colors,
            categories = [<%=ObtenerDatos2()%>],
           
            data = [<%=ObtenerDatos3()%>];
                          
          
          
              
         function setChart(name, categories, data, color) {
            chart.xAxis[0].setCategories(categories, false);
             chart.series[0].remove(false);
            
            chart.addSeries({
                name: name,
                data: data,
                color: color || 'white'
            }, false);
             chart.redraw();

        }

        var chart = $('#panel2').highcharts({
            chart: {
                type: 'bar'
            },
            title: {
                text: name

            },
            subtitle: {
                text: ''
            },
            xAxis: {
                categories: categories
            },
            yAxis: {
                min: 0,
                max: 100,
                tickInterval: 10,
                title: {
                    text: 'Porcentaje'
                }
            },
            plotOptions: {

                bar: {
                    cursor: 'pointer',
                    
                    point: {
                        events: {
                            click:
                                function() {
                                   var drilldown = this.drilldown;
                                    if (drilldown) { // drill down
                                       //alert(drilldown.categories)
                                        setChart(drilldown.name, drilldown.categories, drilldown.data, drilldown.color);

                                    } else { // restore
                                      setChart(name, categories, data);
                                   }

                                    
                            }
                        }
                    },
                    dataLabels: {
                        enabled: true,
                        color: colors[0],
                        style: {
                            fontWeight: 'bold'
                        },
                        formatter: function () {
                            return this.y + '%';
                        }
                    }
                }
            },
            tooltip: {
                formatter: function () {
                    var point = this.point,
                        s = this.x + ':<b>' + this.y + '%</b><br/>';
                    if (point.drilldown) {
                        s += 'Clickkkk ' + point.category ;
                    } else {
                        s += 'Click Volver';
                       
                    }
                    return s;
                }
            },
            series: [{
                name: name,
                data: data,
                color: 'white'
            }],
            exporting: {
                enabled: true
            }
        })
            .highcharts(); // return chart

      
        };



    </script>
  </head>

    <body onload="Carga()">
    <!--Div that will hold the pie chart-->
    <!--<div id="chart_div"></div>-->
   <p >Tablero de Proyectos </p>

        <div id="panel2" style="min-width: 310px; max-width: 800px; height: 400px; margin: 0 auto"></div>

        <input type="button"  value="Volverrrrrrr" onclick="Carga()"/>

  </body>
   </html>
</asp:Content>
