﻿<%@ Page Language="C#" MasterPageFile="~/Anonimo.Master" AutoEventWireup="true" CodeBehind="frmPrincipal.aspx.cs" Inherits="MantenedorWEB.frmPrincipal" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .btnVolver
        {}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%--<br />
    <asp:Button ID="btnAgregar" runat="server" Text="Agregar petición" CssClass="agr-btn"
        onclick="btnAgregar_Click" />
    <br />
    <br />--%>
      <div>
    <h2><br />
        <asp:Label Text="Proyecto" ID="lblTitulo" runat="server" visible = "false"></asp:Label>
    </h2>
    <br />
        <table style="width: 99%">
        <tr>
            <td>
                <asp:Label Text="Nombre del proyecto:" ID="lblNombre" runat="server" visible = "false" CssClass="font"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtNombre" runat="server" Width ="600px" display="none" visible="false" BorderStyle="None"></asp:TextBox>
            </td>
        </tr>
        <tr>    
            <td>
               <asp:Label Text="Descripción del proyecto:" ID="lblDescripcion" runat="server" visible="false" CssClass="font"></asp:Label>
            </td>
            <td>
               <asp:TextBox ID="txtDescripcion" runat="server" Width ="1059px" Height = "27px" 
                     visible = "false" BorderStyle="None"></asp:TextBox>
            </td>
        </tr>
        <tr>    
            <td> 
                <asp:Label Text="Jefe de proyecto:" ID="lblJefe" runat="server" visible = "false" CssClass="font"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtJefeProyecto" runat="server" Width ="600px" visible = "false" BorderStyle="None"></asp:TextBox>
            </td>
        </tr>
        <tr>    
            <td> 
                <asp:Label Text="Area del proyecto:" ID="lblArea" runat="server" visible = "false" CssClass="font"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtAreaProyecto" runat="server" Width ="600px" visible = "false" BorderStyle="None"></asp:TextBox>
            </td>
        </tr>
        </table>
    </div>
    <br />
    
        <asp:GridView ID="grvPeticiones" runat="server" AutoGenerateColumns="False"
        HeaderStyle-CssClass="header"  Width="100%" 
        onselectedindexchanged="grvPeticiones_SelectedIndexChanged" 
        OnRowEditing="grvPeticiones_RowEditing" EnableModelValidation="True" Height="138px" BackColor="White" CssClass="mygrdContent" BorderColor="#9aca3c" PagerStyle-CssClass="pager">
        
    <%--<asp:GridView ID="grvPeticiones" runat="server" AutoGenerateColumns="False" 
        CellPadding="4"
        HeaderStyle-CssClass="alinear" Width="100%" 
        onselectedindexchanged="grvPeticiones_SelectedIndexChanged" 
        OnRowEditing="grvPeticiones_RowEditing" EnableModelValidation="True" Height="138px" BackColor="#9aca3c" CssClass="gridview" BorderColor="#9aca3c" BorderStyle="None" BorderWidth="1px">--%>
        <Columns>
            <asp:BoundField DataField="idPeticion" HeaderText="Petición #" >
                <ControlStyle Width="80px" />
            </asp:BoundField>
            <asp:BoundField DataField="Asunto" HeaderText="Asunto" />
            <asp:BoundField DataField="Descripcion" HeaderText="Descripción" />
            <asp:BoundField DataField="FechaInicio" DataFormatString="{0:d}" HeaderText="Fecha Inicio" />
            <asp:BoundField DataField="FechaFin" DataFormatString="{0:d}" 
                HeaderText="Fecha fin" />
            <asp:BoundField DataField="PorcentajeRealizado" HeaderText="% Avance">
                <ItemStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:CommandField ShowSelectButton="True" EditText="Modificar" SelectText="Ver" 
                ShowEditButton="True" >
            <FooterStyle Wrap="False" />
            <ItemStyle Wrap="False" />
            </asp:CommandField>
        </Columns>
        <HeaderStyle />
        <PagerStyle />
    </asp:GridView>
    <br />
    
    <td align="right"><asp:ImageButton runat="server"   img src="Imagenes/go-back256_24856.png" Width="70px" OnClick="btnVolver_Click" Height="32px" /></td>
                            <%--<asp:Button ID="UpdateButton" runat="server" meta:resourceKey="UpdateButton" OnClick="UpdateButton_Click" CssClass="Updatebutton" />--%>
                           
</asp:Content>
