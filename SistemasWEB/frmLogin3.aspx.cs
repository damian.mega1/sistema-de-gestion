﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using CapaEntidades;
using CapaNegocio;

namespace MantenedorWEB
{
    public partial class frmLogin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(Session["usuario"] != null)
            {
                Response.Redirect("frmPrincipal.aspx");
            }

        }

        protected void btnAceptar_Click(object sender, EventArgs e)
        {
            if (txtUsuario.Text != "" && txtPass.Text != "")
            {
                entUsuario obj = negUsuario.login(txtUsuario.Text, txtPass.Text);
                if (obj != null)
                {
                    Session["usuario"] = obj;
                    Response.Redirect("frmPrincipal.aspx");
                }
                else
                {
                    lblError.Text = "Usuario o Contraseña invalido";
                    lblError.Visible = true;
                }
            }
            else
            {
                lblError.Text = "Faltan ingresar campos";
                lblError.Visible = true;
            }
        }
    }
}
