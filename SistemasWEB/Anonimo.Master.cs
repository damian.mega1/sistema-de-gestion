﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using CapaEntidades;

namespace MantenedorWEB
{
    public partial class Anonimo : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            entUsuario obj = (entUsuario)Session["usuario"];
            if (obj != null)
            {
                nombreUsuario.Text = obj.Nombre + " " + obj.Apellido + "   ";
            }
            else
            {
                Response.Redirect("frmLogin.aspx");
            }
        }

        protected void lbtLogout_Click(object sender, EventArgs e)
        {
            Session.Remove("usuario");
            Response.Redirect("frmLogin.aspx");
        }

        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            Response.Redirect("frmAgregarProy.aspx");
        }

        protected void btnModificar_Click(object sender, EventArgs e)
        {
            if (Request.QueryString["cat"] != null)
            {
                //int cat = Convert.ToInt32(Request.QueryString["cat"]);
                Response.Redirect("frmModificarProy.aspx?idproyecto=" + Request.QueryString["cat"]);
            }
            else
            {
                //MessageBox.Show("Datos erroneos");
                lblErrorAnonimo.Text = "Debe seleccionar un proyecto";
                lblErrorAnonimo.Visible = true;
            }
        }

        protected void btnReporte_Click(object sender, EventArgs e)
        {
            Response.Redirect("frmRreporte.aspx");
        }

        protected void btnUsuarios_Click(object sender, EventArgs e)
        {
            Response.Redirect("frmBuscarUsuarios.aspx");
        }

        protected void btnAgregarUsuario_Click(object sender, EventArgs e)
        {
            Response.Redirect("frmAgregarUsuario.aspx");
        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("frmBuscarUsuarios.aspx");
        }

        protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("frmAgregarUsuario.aspx");
            //Response.Redirect("frmAgregarUsuario.aspx");
        }

        protected void Reporte_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("frmReporte.aspx");
        }

        protected void btnInicio_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("frmPrincipal.aspx");
        }

        protected void peticion_Click(object sender, EventArgs e)
        {
            Response.Redirect("frmAgregar.aspx");
        }

        protected void btnProyectos_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("frmProyecto.aspx");
        }

        protected void btnTablero_Click(object sender, EventArgs e)
        {
            Response.Redirect("frmTablero.aspx");
        }



    }
}
