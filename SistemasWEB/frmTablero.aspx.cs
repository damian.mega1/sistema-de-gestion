﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MantenedorWEB
{
    public partial class frmTablero : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected string ObtenerDatos()
        {
            string strDatos;
                    
            strDatos = "";

            System.Collections.Generic.List<CapaEntidades.entProyecto>
            lista = CapaNegocio.negProyecto.ListarProyectosAvances();
            if (lista != null)
            {
               
                strDatos = "[['Proyecto','Porcentaje'],";

                foreach (CapaEntidades.entProyecto obj in lista)
                {
                    

                    strDatos = strDatos + "[";
                    strDatos = strDatos + "'" + obj.Nombre + "'" + "," + obj.PorcentajeAvance;
                    strDatos = strDatos + "],";

                    
                }

                strDatos = strDatos + "]";
               

            }
               return strDatos;


        }



        protected string ObtenerDatos2()
        {
         
            string strProyectos;
        


            strProyectos = "";
       

            System.Collections.Generic.List<CapaEntidades.entProyecto>
            lista = CapaNegocio.negProyecto.ListarProyectosAvances();
            if (lista != null)
            {

              

                foreach (CapaEntidades.entProyecto obj in lista)
                {



                    strProyectos = strProyectos + "'" + obj.Nombre + "',";
                  
                }

              


            }
            return strProyectos;


        }

        protected string ObtenerDatos3()
        {

           
            string strPorcentaje;
            string strPeticion;
            string strPorcentajePeticion;

            strPorcentaje = "";
            strPeticion = "";
            strPorcentajePeticion = "";

            System.Collections.Generic.List<CapaEntidades.entProyecto>
            lista = CapaNegocio.negProyecto.ListarProyectosAvances();
            if (lista != null)
            {



                foreach (CapaEntidades.entProyecto obj in lista)
                {
                    List<CapaEntidades.entPeticion> listaPet = CapaNegocio.negPeticion.ListarPeticionesPorProyecto(obj.idProyecto);

                    foreach (CapaEntidades.entPeticion objPet in listaPet) {
                        strPeticion = strPeticion +"'"+objPet.Asunto+"',";
                        strPorcentajePeticion = strPorcentajePeticion + objPet.PorcentajeRealizado+ "," ;

                    }

                    strPorcentaje = strPorcentaje + "{";
                    strPorcentaje = strPorcentaje + "y:" + obj.PorcentajeAvance + ", color: colors[3],   drilldown:{name:'"+obj.Nombre+"',categories:[" + strPeticion + "],data:["+ strPorcentajePeticion +"],color:colors[0]}";
                    strPorcentaje = strPorcentaje + "},";
                 
                }

              


            }
            return strPorcentaje;


        }

    }
}