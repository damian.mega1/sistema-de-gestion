﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CapaEntidades
{
    public class entPeticion
    {
        public int idPeticion { get; set; }
        public int refProyecto { get; set; }
        public int refTipo { get; set; }
        public string Asunto { get; set; }
        public string Descripcion { get; set; }
        public int refEstado { get; set; }
        public int refPrioridad { get; set; }
        public int refAsignadaA { get; set; }
        public DateTime FechaInicio { get; set; }
        public DateTime FechaFin { get; set; }
        public float TiempoEstimado { get; set; }
        public int PorcentajeRealizado { get; set; }
    }
}
