﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CapaEntidades
{
    public class entOrgProyecto
    {
        public int idOrganigramaSector { get; set; }
        public int idProyecto { get; set; }
        public String Nombre { get; set; }
    }
}
