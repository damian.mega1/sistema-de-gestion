﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CapaEntidades
{
    public class entProyecto
    {
        public int idProyecto { get; set; }
        public String Nombre { get; set; }
        public String Descripcion { get; set; }
        public int refJefeProyecto { get; set; }
        public int refOrganigramaSectores { get; set; }
        public int PorcentajeAvance { get; set; }
    }
}
