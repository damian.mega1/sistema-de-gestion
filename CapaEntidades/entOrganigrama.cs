﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CapaEntidades
{
    public class entOrganigrama
    {
	 public int idOrganigramaSector { get; set; }
	 public int refPadre { get; set; }
	 public int refDestino { get; set; }
	 public String cAbNombreSector { get; set; }
	 public String cNombreSector { get; set; }
	 public String cRegionMinisterio { get; set; }
	 public String cGrupoMinisterio { get; set; }
	 public Boolean bEsProvincia { get; set; }
	 public Boolean bActivo { get; set; }
     public Boolean bVisible { get; set; }
    }
}
