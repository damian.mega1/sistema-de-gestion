﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CapaEntidades;
using CapaNegocio;

namespace Mantenedor
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            entUsuario obj = negUsuario.login(usuario.Text, password.Text);
            if (obj != null)
            {
                Principal p = new Principal();
                p.Visible = true;
                this.Dispose(false);
            }
            else
            {
                MessageBox.Show("Usted no está registrado");
            }
        }
    }
}
