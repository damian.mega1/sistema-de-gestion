﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CapaEntidades;
using CapaNegocio;

namespace Mantenedor
{
    public partial class Principal : Form
    {
        public Principal()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;
            InicializarInterfaz();
        }
        
        private void Arbol_DoubleClick(object sender, EventArgs e)
        {
            if (!Arbol.SelectedNode.Name.Equals(""))
            {
                categoria.DataSource = negProyecto.ListarProyectos();
                categoria.DisplayMember = "Descripcion";
                categoria.ValueMember = "idCategoria";
                entPeticion obj = negPeticion.BuscarPeticion(Convert.ToInt32(Arbol.SelectedNode.Name));
                nombre.Text = obj.Asunto;
                detalle.Text = obj.Descripcion;
                precio.Text = obj.refEstado.ToString();
                stock.Text = obj.refPrioridad.ToString();
                categoria.SelectedValue = obj.refAsignadaA;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            entPeticion obj = new entPeticion();
            try
            {
                obj.Descripcion = detalle.Text;
                obj.refAsignadaA = Convert.ToInt32(categoria.SelectedValue.ToString());
                obj.idPeticion = Convert.ToInt32(Arbol.SelectedNode.Name.ToString());
                obj.Asunto = nombre.Text;
                obj.refEstado = Convert.ToInt32(precio.Text);
                obj.refPrioridad = Convert.ToInt32(stock.Text);
                if (negPeticion.ModificarPeticion(obj) > 0)
                {
                    MessageBox.Show("Modificado con exito");
                    Arbol.Nodes.Clear();
                    InicializarInterfaz();
                }
                else
                {
                    MessageBox.Show("Error al intentar modificar");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Datos erroneos");
            }

        }

        private void InicializarInterfaz()
        {
            List<entProyecto> listaC = negProyecto.ListarProyectos();
            foreach (entProyecto c in listaC)
            {
                TreeNode padre = new TreeNode(c.Descripcion);
                List<entPeticion> listaP = negPeticion.ListarPeticionesPorProyecto(c.idProyecto);
                foreach (entPeticion p in listaP)
                {
                    TreeNode hijo = new TreeNode(p.Asunto);
                    hijo.Name = p.idPeticion.ToString();
                    padre.Nodes.Add(hijo);
                }
                Arbol.Nodes.Add(padre);
            }
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            AgregarProducto ap = new AgregarProducto();
            ap.Visible = true;
            this.Dispose(false);
        }
    }
}
