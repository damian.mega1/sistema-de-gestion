﻿<%@ Page Language="C#" MasterPageFile="~/Anonimo.Master" AutoEventWireup="true" CodeBehind="frmPrincipal.aspx.cs" Inherits="MantenedorWEB.frmPrincipal" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <asp:Button ID="btnAgregar" runat="server" Text="Agregar petición" 
        onclick="btnAgregar_Click" />
    <br />
    <br />
    <asp:GridView ID="grvPeticiones" runat="server" AutoGenerateColumns="False" 
        CellPadding="4" ForeColor="#333333" GridLines="None" 
        HeaderStyle-CssClass="alinear" Width="740px" 
        onselectedindexchanged="grvPeticiones_SelectedIndexChanged" 
        OnRowEditing="grvPeticiones_RowEditing">
        <RowStyle BackColor="#EFF3FB" />
        <Columns>
            <asp:BoundField DataField="idPeticion" HeaderText="Petición #" >
                <ControlStyle Width="80px" />
            </asp:BoundField>
            <asp:BoundField DataField="Asunto" HeaderText="Asunto" />
            <asp:BoundField DataField="Descripcion" HeaderText="Descripción" />
            <asp:BoundField DataField="FechaFin" DataFormatString="{0:d}" 
                HeaderText="Fecha fin" />
            <asp:BoundField DataField="PorcentajeRealizado" HeaderText="% Avance">
                <ItemStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:CommandField ShowSelectButton="True" EditText="Modificar" SelectText="Ver" 
                ShowEditButton="True" />
        </Columns>
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <EditRowStyle BackColor="#2461BF" />
        <AlternatingRowStyle BackColor="White" />
    </asp:GridView>
</asp:Content>
