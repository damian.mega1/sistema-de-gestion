﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using CapaEntidades;
using CapaNegocio;
using System.Collections.Generic;

namespace MantenedorWEB
{
    public partial class frmAgregarUsuario : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                List<entFuncion> lista = negFuncion.ListarFunciones();
                foreach (entFuncion obj in lista)
                {
                    ListItem item = new ListItem(obj.Descripcion, obj.idFuncion.ToString());
                    cmbFuncion.Items.Add(item);
                }

                List<entTipoUsuario> listaTU = negTipoUsuario.ListarTiposUsuario();
                foreach (entTipoUsuario obj in listaTU)
                {
                    ListItem item = new ListItem(obj.Descripcion, obj.idTipoUsuario.ToString());
                    cmbTipoUsuario.Items.Add(item);
                }
            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            if (txtApellido.Text != "" && txtNombre.Text != "" && txtUsuario.Text != "" && txtPassword.Text != "")
            {
                entUsuario obj = new entUsuario();
                obj.Apellido = txtApellido.Text;
                obj.Nombre = txtNombre.Text;
                obj.Usuario = txtUsuario.Text;
                obj.Password = txtPassword.Text;
                obj.refFuncion = Convert.ToInt32(cmbFuncion.SelectedValue);
                obj.refTipoUsuario = Convert.ToInt32(cmbTipoUsuario.SelectedValue);
                if (negUsuario.AgregarUsuario(obj) == 1)
                {
                    Response.Redirect("frmPrincipal.aspx");
                }
                else
                {
                    lblError.Text = "No se pudo agregar el usuario";
                    lblError.Visible = true;
                }
            }
            else
            {
                lblError.Text = "Faltan ingresar campos";
                lblError.Visible = true;
            }
        }
    }
}
