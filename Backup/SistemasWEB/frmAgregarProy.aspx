﻿<%@ Page Language="C#" MasterPageFile="~/Anonimo.Master" AutoEventWireup="true" CodeBehind="frmAgregarProy.aspx.cs" Inherits="MantenedorWEB.frmAgregarProy" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div id="abrigo_formulario">
    <h2>Nuevo proyecto</h2>
    <br />
        <table>
        <tr>
            <td>Nombre: </td>
            <td>
                <asp:TextBox ID="txtNombre" runat="server" Width ="430px"></asp:TextBox>
            </td>
        </tr>
        <tr>    
            <td>Descripción: </td>
            <td>
               <asp:TextBox ID="txtDescripcion" runat="server" Width ="430px" Height = "90px" 
                    TextMode="MultiLine"></asp:TextBox>
            </td>
        </tr>
        <tr>    
            <td>Jefe de proyecto: </td>
            <td>
               <asp:DropDownList ID="cmbUsuario" runat="server" Width ="200px"></asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblError" runat="server" Text="" Visible="false" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:Button ID="btnGuardar" runat="server" 
                    Text="Guardar" Width="170px" onclick="btnGuardar_Click" /></td>
        </tr>    
        </table>
    </div>

</asp:Content>
