﻿<%@ Page Language="C#" MasterPageFile="~/Anonimo.Master" AutoEventWireup="true" CodeBehind="frmVerUsuario.aspx.cs" Inherits="MantenedorWEB.frmVerUsuario" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="abrigo_formulario">
    <h2>Ver Usuario</h2>
        <table>
        <tr>    
            <td>Apellido: </td>
            <td>
                <asp:TextBox ID="txtApellido" runat="server" Width ="230px"  Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr>    
            <td>Nombres: </td>
            <td>
                <asp:TextBox ID="txtNombre" runat="server" Width ="230px" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>Usuario: </td>
            <td>
                <asp:TextBox ID="txtUsuario" runat="server" Width ="230px" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr>    
            <td>Password: </td>
            <td>
               <asp:TextBox ID="txtPassword" runat="server" Width ="230px" Enabled="false"></asp:TextBox>
            </td>
        </tr>
        <tr>    
            <td>Función: </td>
            <td>
                <asp:DropDownList ID="cmbFuncion" runat="server" Width ="230px" Enabled="false"></asp:DropDownList>
            </td>
        </tr>
        <tr>    
            <td>Tipo de Usuario: </td>
            <td>
                <asp:DropDownList ID="cmbTipoUsuario" runat="server" Width ="230px" Enabled="false"></asp:DropDownList>
            </td>
        </tr>
       <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblError" runat="server" Text="" Visible="false" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        </table>
    </div>

</asp:Content>
