﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using CapaEntidades;
using CapaNegocio;
using System.Collections.Generic;

namespace MantenedorWEB
{
    public partial class frmVerUsuario : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["idUsuario"] != null)
                {
                    int idusuario = Convert.ToInt32(Request.QueryString["idUsuario"]);
                    entUsuario obj = negUsuario.BuscarUsuario(idusuario);
                    txtApellido.Text = obj.Apellido;
                    txtNombre.Text = obj.Nombre;
                    txtUsuario.Text = obj.Usuario;
                    txtPassword.Text = obj.Password;
                    List<entFuncion> lista = negFuncion.ListarFunciones();
                    foreach (entFuncion c in lista)
                    {
                        ListItem item = new ListItem(c.Descripcion, c.idFuncion.ToString());
                        cmbFuncion.Items.Add(item);
                    }

                    List<entTipoUsuario> listaTU = negTipoUsuario.ListarTiposUsuario();
                    foreach (entTipoUsuario c in listaTU)
                    {
                        ListItem item = new ListItem(c.Descripcion, c.idTipoUsuario.ToString());
                        cmbTipoUsuario.Items.Add(item);
                    }
                }
                else
                {
                    Response.Redirect("frmPrincipal.aspx");
                }
            }

        }
    }
}
