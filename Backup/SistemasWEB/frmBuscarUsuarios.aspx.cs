﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using CapaNegocio;

namespace MantenedorWEB
{
    public partial class frmBuscarUsuarios : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
                grvUsuarios.DataSource = negUsuario.ListarUsuarios();
                grvUsuarios.DataBind();
        }

        protected void grvUsuarios_SelectedIndexChanged(object sender, EventArgs e)
        {
            Response.Redirect("frmVerUsuario.aspx?idUsuario=" + grvUsuarios.Rows[grvUsuarios.SelectedIndex].Cells[0].Text);
        }

        protected void grvUsuarios_RowEditing(object sender, EventArgs e)
        {
            Response.Redirect("frmModificarUsuario.aspx?idUsuario=" + grvUsuarios.Rows[(e as GridViewEditEventArgs).NewEditIndex].Cells[0].Text);
        }
    }
}
