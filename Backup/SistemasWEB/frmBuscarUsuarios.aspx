﻿<%@ Page Language="C#" MasterPageFile="~/Anonimo.Master" AutoEventWireup="true" CodeBehind="frmBuscarUsuarios.aspx.cs" Inherits="MantenedorWEB.frmBuscarUsuarios" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>Usuarios</h2>

    <asp:GridView ID="grvUsuarios" runat="server" AutoGenerateColumns="False" 
        CellPadding="4" ForeColor="#333333" GridLines="None" 
        HeaderStyle-CssClass="alinear" Width="740px" 
        onselectedindexchanged="grvUsuarios_SelectedIndexChanged" 
        OnRowEditing="grvUsuarios_RowEditing">
        <RowStyle BackColor="#EFF3FB" />
        <Columns>
            <asp:BoundField DataField="idUsuario" HeaderText="# Usuario" />
            <asp:BoundField DataField="Apellido" HeaderText="Apellido" >
                <ControlStyle Width="80px" />
            </asp:BoundField>
            <asp:BoundField DataField="Nombre" HeaderText="Nombre" />
            <asp:BoundField DataField="Funcion" HeaderText="Función" />
            <asp:BoundField DataField="TipoUsuario" 
                HeaderText="Tipo de usuario" />
            <asp:CommandField ShowSelectButton="True" EditText="Modificar" SelectText="Ver" 
                ShowEditButton="True" />
        </Columns>
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <EditRowStyle BackColor="#2461BF" />
        <AlternatingRowStyle BackColor="White" />
    </asp:GridView>

</asp:Content>
