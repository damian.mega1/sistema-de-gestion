﻿<%@ Page Language="C#" MasterPageFile="~/Anonimo.Master" AutoEventWireup="true" CodeBehind="frmVerPeticion.aspx.cs" Inherits="MantenedorWEB.frmVerPeticion" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div id="abrigo_formulario">
    <h2>Ver petición</h2>
        <table>
        <tr>    
            <td>Proyecto: </td>
            <td>
                <asp:DropDownList ID="cmbProyectos" runat="server" Width ="200px"></asp:DropDownList>
            </td>
        </tr>
        <tr>    
            <td>Tipo de petición: </td>
            <td>
                <asp:DropDownList ID="cmbTipoPeticion" runat="server" Width ="200px"></asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>Asunto: </td>
            <td>
                <asp:TextBox ID="txtAsunto" runat="server" Width ="430px"></asp:TextBox>
            </td>
        </tr>
        <tr>    
            <td>Descripción: </td>
            <td>
               <asp:TextBox ID="txtDescripcion" runat="server" Width ="430px" Height = "90px" 
                    TextMode="MultiLine"></asp:TextBox>
            </td>
        </tr>
        <tr>    
            <td>Estado peticion: </td>
            <td>
                <asp:DropDownList ID="cmbEstadoPeticion" runat="server" Width ="200px"></asp:DropDownList>
            </td>
        </tr>
        <tr>    
            <td>Prioridad: </td>
            <td>
                <asp:DropDownList ID="cmbPrioridadPeticion" runat="server" Width ="200px"></asp:DropDownList>
            </td>
        </tr>
        <tr>    
            <td>Asignada a: </td>
            <td>
                <asp:DropDownList ID="cmbAsignadaA" runat="server" Width ="200px"></asp:DropDownList>
            </td>
        </tr>
        <tr>    
            <td>Fecha de inicio: </td>
            <td>
                <asp:TextBox ID="txtFechaInicio" runat="server" Width ="200px"></asp:TextBox>
                
                <asp:Calendar ID="calFechaInicio" runat="server" Width ="200px" Visible="false"></asp:Calendar>
            </td>
        </tr>
        <tr>    
            <td>Fecha de finalización: </td>
            <td>
                <asp:TextBox ID="txtFechaFin" runat="server" Width ="200px"></asp:TextBox>
                
                <asp:Calendar ID="calFechaFin" runat="server" Width ="200px" Visible="false"></asp:Calendar>
            </td>
        </tr>
        <tr>    
            <td>Tiempo Estimado (días): </td>
            <td>
               <asp:TextBox ID="txtTiempoEstimado" runat="server" Width ="200px" ></asp:TextBox>
            </td>
        </tr>
        <tr>    
            <td>% Realizado: </td>
            <td>
               <asp:TextBox ID="txtPorcentajeRealizado" runat="server" Width ="200px" ></asp:TextBox>
            </td>
        </tr>
       <tr>
            <td colspan="2" align="center">
                <asp:Label ID="lblError" runat="server" Text="" Visible="false" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        </table>
    </div>
</asp:Content>
