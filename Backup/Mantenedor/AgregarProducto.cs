﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CapaEntidades;
using CapaNegocio;

namespace Mantenedor
{
    public partial class AgregarProducto : Form
    {
        public AgregarProducto()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;
            categoria.DataSource = negProyecto.ListarProyectos();
            categoria.DisplayMember = "Descripcion";
            categoria.ValueMember = "idcategoria";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                entPeticion obj = new entPeticion();
                obj.Descripcion = detalle.Text;
                obj.refAsignadaA = Convert.ToInt32(categoria.SelectedValue.ToString());
                obj.Asunto = nombre.Text;
                obj.refEstado = Convert.ToInt32(precio.Text.ToString());
                obj.refPrioridad = Convert.ToInt32(stock.Text.ToString());
                if (negPeticion.AgregarPeticion(obj) > 0)
                {
                    MessageBox.Show("Agregado con exito");
                    Principal p = new Principal();
                    this.Dispose(false);
                    p.Visible = true;
                }
                else
                {
                    MessageBox.Show("Error al agregar el producto");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Datos erroneos");
            }
        }
    }
}
