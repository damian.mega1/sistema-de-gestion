﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CapaEntidades
{
    public class entTipoUsuario
    {
        public int idTipoUsuario { get; set; }
        public string Descripcion { get; set; }
        public int Permiso { get; set; }
    }
}
