﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CapaEntidades
{
    public class entUsuario
    {
        public int idUsuario { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Usuario { get; set; }
        public string Password { get; set; }
        public int refFuncion { get; set; }
        public int refTipoUsuario { get; set; }
        public string Funcion { get; set; }
        public string TipoUsuario { get; set; }
        public int Permiso { get; set; }
    }
}
