﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CapaEntidades;
using CapaDatos;

namespace CapaNegocio
{
    public class negUsuario
    {
        public static entUsuario login(string usuario, string password)
        {
            return daoUsuario.login(usuario, password);
        }
        public static List<entUsuario> ListarUsuarios()
        {
            return daoUsuario.ListarUsuarios();
        }
        public static List<entUsuario> ListarUsuariosCompleto()
        {
            return daoUsuario.ListarUsuariosCompleto();
        }
        public static int AgregarUsuario(entUsuario obj)
        {
            return daoUsuario.AgregarUsuario(obj);
        }
        public static entUsuario BuscarUsuario(int idUsuario)
        {
            return daoUsuario.BuscarUsuario(idUsuario);
        }
        public static int ModificarUsuario(entUsuario obj)
        {
            return daoUsuario.ModificarUsuario(obj);
        }
    }
}
