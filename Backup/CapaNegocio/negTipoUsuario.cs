﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CapaEntidades;
using CapaDatos;

namespace CapaNegocio
{
    public class negTipoUsuario
    {
        public static List<entTipoUsuario> ListarTiposUsuario()
        {
            return daoTipoUsuario.ListarTiposUsuario();
        }
    }
}
