﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CapaEntidades;
using System.Data.SqlClient;
using System.Data;

namespace CapaDatos
{
    public class daoUsuario
    {
        public static entUsuario login(string usuario, string password)
        {
            entUsuario obj = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                Conexion cn = new Conexion();
                SqlConnection cnx = cn.conectar();
                cmd = new SqlCommand("VerificarUsuario", cnx);
                cmd.Parameters.AddWithValue("@Usuario", usuario);
                cmd.Parameters.AddWithValue("@Password", password);
                cmd.CommandType = CommandType.StoredProcedure;
                cnx.Open();
                dr = cmd.ExecuteReader();
                obj = new entUsuario();
                dr.Read();
                obj.idUsuario = Convert.ToInt32(dr["idUsuario"].ToString());
                obj.Apellido = dr["Apellido"].ToString();
                obj.Nombre = dr["Nombre"].ToString();
                obj.Password = dr["Password"].ToString();
                obj.Usuario = dr["Usuario"].ToString();
                obj.refFuncion = Convert.ToInt32(dr["refFuncion"].ToString());
                obj.refTipoUsuario = Convert.ToInt32(dr["refTipoUsuario"].ToString());
                obj.Permiso = Convert.ToInt32(dr["Permiso"].ToString());
            }
            catch (Exception e)
            {
                obj = null;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return obj;
        }
        public static List<entUsuario> ListarUsuarios()
        {
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<entUsuario> lista = null;

            try
            {
                Conexion cn = new Conexion();
                SqlConnection cnx = cn.conectar();
                cmd = new SqlCommand("ListarUsuarios", cnx);
                cmd.CommandType = CommandType.StoredProcedure;
                cnx.Open();
                dr = cmd.ExecuteReader();
                lista = new List<entUsuario>();
                while (dr.Read())
                {
                    entUsuario c = new entUsuario();
                    c.idUsuario = Convert.ToInt32(dr["idUsuario"].ToString());
                    c.Nombre = dr["Nombre"].ToString();
                    c.Apellido = dr["Apellido"].ToString();
                    c.Usuario = dr["Usuario"].ToString();
                    c.Password = dr["Password"].ToString();
                    c.refFuncion = Convert.ToInt32(dr["refFuncion"].ToString());
                    c.refTipoUsuario = Convert.ToInt32(dr["refTipoUsuario"].ToString());
                    c.Funcion = dr["Funcion"].ToString();
                    c.TipoUsuario = dr["TipoUsuario"].ToString();
                    c.Permiso = Convert.ToInt32(dr["Permiso"].ToString());
                    lista.Add(c);
                }
            }
            catch (Exception e)
            {
                lista = null;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return lista;
        }
        public static List<entUsuario> ListarUsuariosCompleto()
        {
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<entUsuario> lista = null;

            try
            {
                Conexion cn = new Conexion();
                SqlConnection cnx = cn.conectar();
                cmd = new SqlCommand("ListarUsuariosCompleto", cnx);
                cmd.CommandType = CommandType.StoredProcedure;
                cnx.Open();
                dr = cmd.ExecuteReader();
                lista = new List<entUsuario>();
                while (dr.Read())
                {
                    entUsuario c = new entUsuario();
                    c.idUsuario = Convert.ToInt32(dr["idUsuario"].ToString());
                    c.Nombre = dr["Nombre"].ToString();
                    c.Apellido = dr["Apellido"].ToString();
                    c.Usuario = dr["Usuario"].ToString();
                    c.Password = dr["Password"].ToString();
                    c.refFuncion = Convert.ToInt32(dr["refFuncion"].ToString());
                    c.refTipoUsuario = Convert.ToInt32(dr["refTipoUsuario"].ToString());
                    c.Permiso = Convert.ToInt32(dr["Permiso"].ToString());
                    c.Funcion = dr["Funcion"].ToString();
                    c.TipoUsuario = dr["TipoUsuario"].ToString();
                    lista.Add(c);
                }
            }
            catch (Exception e)
            {
                lista = null;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return lista;
        }
        public static int AgregarUsuario(entUsuario obj)
        {
            int Indicador = 0;
            SqlCommand cmd = null;
            try
            {
                Conexion cn = new Conexion();
                SqlConnection cnx = cn.conectar();
                cmd = new SqlCommand("AgregarUsuario", cnx);
                cmd.Parameters.AddWithValue("@Nombre", obj.Nombre);
                cmd.Parameters.AddWithValue("@Apellido", obj.Apellido);
                cmd.Parameters.AddWithValue("@Usuario", obj.Usuario);
                cmd.Parameters.AddWithValue("@Password", obj.Password);
                cmd.Parameters.AddWithValue("@refFuncion", obj.refFuncion);
                cmd.Parameters.AddWithValue("@refTipoUsuario", obj.refTipoUsuario);
                cmd.CommandType = CommandType.StoredProcedure;
                cnx.Open();
                cmd.ExecuteNonQuery();
                Indicador = 1;
            }
            catch (Exception e)
            {
                Indicador = 0;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return Indicador;
        }
        public static entUsuario BuscarUsuario(int idUsuario)
        {
            entUsuario obj = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                Conexion cn = new Conexion();
                SqlConnection cnx = cn.conectar();
                cmd = new SqlCommand("BuscarUsuario", cnx);
                cmd.Parameters.AddWithValue("@IdUsuario", idUsuario);
                cmd.CommandType = CommandType.StoredProcedure;
                cnx.Open();
                dr = cmd.ExecuteReader();
                obj = new entUsuario();
                dr.Read();
                obj.idUsuario = Convert.ToInt32(dr["idUsuario"].ToString());
                obj.Apellido = dr["Apellido"].ToString();
                obj.Nombre = dr["Nombre"].ToString();
                obj.Usuario = dr["Usuario"].ToString();
                obj.Password = dr["Password"].ToString();
                obj.refFuncion = Convert.ToInt32(dr["refFuncion"].ToString());
                obj.refTipoUsuario = Convert.ToInt32(dr["refTipoUsuario"].ToString());
                obj.Funcion = dr["Funcion"].ToString();
                obj.TipoUsuario = dr["TipoUsuario"].ToString();
                obj.Permiso = Convert.ToInt32(dr["Permiso"].ToString());
            }
            catch (Exception e)
            {
                obj = null;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return obj;
        }
        public static int ModificarUsuario(entUsuario obj)
        {
            int Indicador = 0;
            SqlCommand cmd = null;
            try
            {
                Conexion cn = new Conexion();
                SqlConnection cnx = cn.conectar();
                cmd = new SqlCommand("ModificarUsuario", cnx);
                cmd.Parameters.AddWithValue("@idUsuario", obj.idUsuario);
                cmd.Parameters.AddWithValue("@Apellido", obj.Apellido);
                cmd.Parameters.AddWithValue("@Nombre", obj.Nombre);
                cmd.Parameters.AddWithValue("@Usuario", obj.Usuario);
                cmd.Parameters.AddWithValue("@Password", obj.Password);
                cmd.Parameters.AddWithValue("@refFuncion", obj.refFuncion);
                cmd.Parameters.AddWithValue("@refTipoUsuario", obj.refTipoUsuario);
                cmd.CommandType = CommandType.StoredProcedure;
                cnx.Open();
                cmd.ExecuteNonQuery();
                Indicador = 1;
            }
            catch (Exception e)
            {
                Indicador = 0;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return Indicador;
        }
    }
}
