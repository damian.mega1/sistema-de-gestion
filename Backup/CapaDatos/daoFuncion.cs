﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CapaEntidades;
using System.Data.SqlClient;
using System.Data;

namespace CapaDatos
{
    public class daoFuncion
    {
        public static List<entFuncion> ListarFunciones()
        {
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<entFuncion> lista = null;

            try
            {
                Conexion cn = new Conexion();
                SqlConnection cnx = cn.conectar();
                cmd = new SqlCommand("ListarFunciones", cnx);
                cmd.CommandType = CommandType.StoredProcedure;
                cnx.Open();
                dr = cmd.ExecuteReader();
                lista = new List<entFuncion>();
                while (dr.Read())
                {
                    entFuncion c = new entFuncion();
                    c.idFuncion = Convert.ToInt32(dr["idFuncion"].ToString());
                    c.Descripcion = dr["Descripcion"].ToString();
                    lista.Add(c);
                }
            }
            catch (Exception e)
            {
                lista = null;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return lista;
        }
    }
}
