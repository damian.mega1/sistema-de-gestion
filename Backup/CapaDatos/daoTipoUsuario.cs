﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CapaEntidades;
using System.Data.SqlClient;
using System.Data;

namespace CapaDatos
{
    public class daoTipoUsuario
    {
        public static List<entTipoUsuario> ListarTiposUsuario()
        {
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<entTipoUsuario> lista = null;

            try
            {
                Conexion cn = new Conexion();
                SqlConnection cnx = cn.conectar();
                cmd = new SqlCommand("ListarTiposUsuario", cnx);
                cmd.CommandType = CommandType.StoredProcedure;
                cnx.Open();
                dr = cmd.ExecuteReader();
                lista = new List<entTipoUsuario>();
                while (dr.Read())
                {
                    entTipoUsuario c = new entTipoUsuario();
                    c.idTipoUsuario = Convert.ToInt32(dr["idTipoUsuario"].ToString());
                    c.Descripcion = dr["Descripcion"].ToString();
                    c.Permiso =  Convert.ToInt32(dr["Permiso"].ToString());
                    lista.Add(c);
                }
            }
            catch (Exception e)
            {
                lista = null;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return lista;
        }
    }
}
