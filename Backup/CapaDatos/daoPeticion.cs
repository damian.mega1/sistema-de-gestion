﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CapaEntidades;
using System.Data.SqlClient;
using System.Data;

namespace CapaDatos
{
    public class daoPeticion
    {
        public static List<entPeticion> ListarPeticionesPorProyecto(int idProyecto)
        {
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<entPeticion> lista = null;

            try
            {
                Conexion cn = new Conexion();
                SqlConnection cnx = cn.conectar();
                cmd = new SqlCommand("ListarPeticionesPorProyecto", cnx);
                cmd.Parameters.AddWithValue("idProyecto",idProyecto);
                cmd.CommandType = CommandType.StoredProcedure;
                cnx.Open();
                dr = cmd.ExecuteReader();
                lista = new List<entPeticion>();
                while (dr.Read())
                {
                    entPeticion p = new entPeticion();
                    p.idPeticion = Convert.ToInt32(dr["idPeticion"].ToString());
                    p.refProyecto = Convert.ToInt32(dr["refProyecto"].ToString());
                    p.refTipo = Convert.ToInt32(dr["refTipo"].ToString());
                    p.Asunto = dr["Asunto"].ToString();
                    p.Descripcion = dr["Descripcion"].ToString();
                    p.refEstado = Convert.ToInt32(dr["refEstado"].ToString());
                    p.refPrioridad = Convert.ToInt32(dr["refPrioridad"].ToString());
                    p.refAsignadaA = Convert.ToInt32(dr["refAsignadoA"].ToString());
                    p.FechaInicio = Convert.ToDateTime(dr["FechaInicio"].ToString());
                    p.FechaFin = Convert.ToDateTime(dr["FechaFin"].ToString());
                    p.TiempoEstimado = float.Parse(dr["TiempoEstimado"].ToString());
                    p.PorcentajeRealizado = Convert.ToInt32(dr["PorcentajeRealizado"].ToString());
                    lista.Add(p);
                }
            }
            catch (Exception e)
            {
                lista = null;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return lista;
        }

        public static entPeticion BuscarPeticion(int idPeticion)
        {
            entPeticion obj = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                Conexion cn = new Conexion();
                SqlConnection cnx = cn.conectar();
                cmd = new SqlCommand("BuscarPeticion", cnx);
                cmd.Parameters.AddWithValue("@IdPeticion", idPeticion);
                cmd.CommandType = CommandType.StoredProcedure;
                cnx.Open();
                dr = cmd.ExecuteReader();
                obj = new entPeticion();
                dr.Read();
                obj.idPeticion = Convert.ToInt32(dr["idPeticion"].ToString());
                obj.refProyecto = Convert.ToInt32(dr["refProyecto"].ToString());
                obj.refTipo = Convert.ToInt32(dr["refTipo"].ToString());
                obj.Asunto = dr["Asunto"].ToString();
                obj.Descripcion = dr["Descripcion"].ToString();
                obj.refEstado = Convert.ToInt32(dr["refEstado"].ToString());
                obj.refPrioridad = Convert.ToInt32(dr["refPrioridad"].ToString());
                obj.refAsignadaA = Convert.ToInt32(dr["refAsignadoA"].ToString());
                obj.FechaInicio = Convert.ToDateTime(dr["FechaInicio"].ToString());
                obj.FechaFin = Convert.ToDateTime(dr["FechaFin"].ToString());
                obj.TiempoEstimado = float.Parse(dr["TiempoEstimado"].ToString());
                obj.PorcentajeRealizado = Convert.ToInt32(dr["PorcentajeRealizado"].ToString());
            }
            catch (Exception e)
            {
                obj = null;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return obj;
        }
        public static int ModificarPeticion(entPeticion obj)
        {
            int Indicador = 0;
            SqlCommand cmd = null;
            try
            {
                Conexion cn = new Conexion();
                SqlConnection cnx = cn.conectar();
                cmd = new SqlCommand("ModificarPeticion", cnx);
                cmd.Parameters.AddWithValue("@idPeticion", obj.idPeticion);
                cmd.Parameters.AddWithValue("@refProyecto", obj.refProyecto);
                cmd.Parameters.AddWithValue("@refTipo", obj.refTipo);
                cmd.Parameters.AddWithValue("@Asunto", obj.Asunto);
                cmd.Parameters.AddWithValue("@Descripcion",obj.Descripcion);
                cmd.Parameters.AddWithValue("@refEstado", obj.refEstado);
                cmd.Parameters.AddWithValue("@refPrioridad", obj.refPrioridad);
                cmd.Parameters.AddWithValue("@refAsignadoA", obj.refAsignadaA);
                cmd.Parameters.AddWithValue("@FechaInicio", obj.FechaInicio);
                cmd.Parameters.AddWithValue("@FechaFin", obj.FechaFin);
                cmd.Parameters.AddWithValue("@TiempoEstimado", obj.TiempoEstimado);
                cmd.Parameters.AddWithValue("@PorcentajeRealizado", obj.PorcentajeRealizado);
                cmd.CommandType = CommandType.StoredProcedure;
                cnx.Open();
                cmd.ExecuteNonQuery();
                Indicador = 1;
            }
            catch (Exception e)
            {
                Indicador = 0;
            }
            finally
            {
                cmd.Connection.Close(); 
            }
            return Indicador;
        }
        public static int AgregarPeticion(entPeticion obj)
        {
            int Indicador = 0;
            SqlCommand cmd = null;
            try
            {
                Conexion cn = new Conexion();
                SqlConnection cnx = cn.conectar();
                cmd = new SqlCommand("AgregarPeticion", cnx);
                cmd.Parameters.AddWithValue("@refProyecto", obj.refProyecto);
                cmd.Parameters.AddWithValue("@refTipo", obj.refTipo);
                cmd.Parameters.AddWithValue("@Asunto", obj.Asunto);
                cmd.Parameters.AddWithValue("@Descripcion", obj.Descripcion);
                cmd.Parameters.AddWithValue("@refEstado", obj.refEstado);
                cmd.Parameters.AddWithValue("@refPrioridad", obj.refPrioridad);
                cmd.Parameters.AddWithValue("@refAsignadoA", obj.refAsignadaA);
                cmd.Parameters.AddWithValue("@FechaInicio", obj.FechaInicio);
                cmd.Parameters.AddWithValue("@FechaFin", obj.FechaFin);
                cmd.Parameters.AddWithValue("@TiempoEstimado", obj.TiempoEstimado);
                cmd.Parameters.AddWithValue("@PorcentajeRealizado", obj.PorcentajeRealizado);
                cmd.CommandType = CommandType.StoredProcedure;
                cnx.Open();
                cmd.ExecuteNonQuery();
                Indicador = 1;
            }
            catch (Exception e)
            {
                Indicador = 0;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return Indicador;
        }
    }
}
