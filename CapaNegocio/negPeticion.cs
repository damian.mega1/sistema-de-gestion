﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CapaEntidades;
using CapaDatos;

namespace CapaNegocio
{
    public class negPeticion
    {
        public static List<entPeticion> ListarPeticionesPorProyecto(int idProyecto)
        {
            return daoPeticion.ListarPeticionesPorProyecto(idProyecto);
        }

        public static entPeticion BuscarPeticion(int idPeticion)
        {
            return daoPeticion.BuscarPeticion(idPeticion);
        }

        public static int ModificarPeticion(entPeticion obj)
        {
            return daoPeticion.ModificarPeticion(obj);
        }

        public static int AgregarPeticion(entPeticion obj)
        {
            return daoPeticion.AgregarPeticion(obj);
        }
    }
}
