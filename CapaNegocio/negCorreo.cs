﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using CapaEntidades;

namespace CapaNegocio
{
    public class CCorreo
    {
        Boolean estado = true;
        string merror;
        public CCorreo(String destinatario, String asunto, String mensaje)
        {
            MailMessage correo = new MailMessage();
            SmtpClient protocolo = new SmtpClient();

            correo.To.Add(destinatario);

            correo.From = new MailAddress (GlobalVariables.GlobalMailA, "Sebastian Vicente", System.Text.Encoding.UTF8);
            correo.Subject = asunto;
            correo.SubjectEncoding = System.Text.Encoding.UTF8;
            correo.Body = mensaje;
            correo.BodyEncoding = System.Text.Encoding.UTF8;
            correo.IsBodyHtml = false;

            protocolo.Credentials = new System.Net.NetworkCredential("svicente@mosp.gba.gov.ar", "ciganoseu7");
            //protocolo.Port = 587;
            protocolo.Port = 25;
            //protocolo.Host = "smtp.hotmail";
            protocolo.Host = "mail.minfra.gba.gov.ar";
            protocolo.EnableSsl = false;

            try
            {
                protocolo.Send(correo);
            }
            catch (SmtpException error)
            {
                estado = false;
                merror = error.Message.ToString();
            }
        }
        public Boolean Estado
        {
            get { return estado; }
        }

        public String mensaje_error
        {
            get { return merror; }
        }
    }

}