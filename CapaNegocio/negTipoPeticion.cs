﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CapaEntidades;
using CapaDatos;

namespace CapaNegocio
{
    public class negTipoPeticion
    {
        public static List<entTipoPeticion> ListarTiposPeticion()
        {
            return daoTipoPeticion.ListarTiposPeticion();
        }
    }
}
