﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CapaEntidades;
using CapaDatos;

namespace CapaNegocio
{
    public class negOrganigrama
    {
        public static List<entOrganigrama> ListarOrganigrama()
        {
            return daoOrganigrama.ListarOrganigrama();
        }
        public static entOrganigrama BuscarOrganigrama(int idOrganigramaSector)
        {
            return daoOrganigrama.BuscarOrganigrama(idOrganigramaSector);
        }
    }
}
