﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CapaEntidades;
using CapaDatos;

namespace CapaNegocio
{
    public class negProyecto
    {
        public static List<entProyecto> ListarProyectos()
        {
            return daoProyecto.ListarProyectos();
        }
        public static int AgregarProyecto(entProyecto obj)
        {
            return daoProyecto.AgregarProyecto(obj);
        }
        public static entProyecto BuscarProyecto(int idProyecto)
        {
            return daoProyecto.BuscarProyecto(idProyecto);
        }
        public static int ModificarProyecto(entProyecto obj)
        {
            return daoProyecto.ModificarProyecto(obj);
        }


        public static List<entProyecto> ListarProyectosAvances()
        {
            return daoProyecto.ListarProyectosAvances();
        }

    }
}
